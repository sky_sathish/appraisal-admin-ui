import { Component } from '@angular/core';
import {ApprisalFunctionalAttributeService} from '../../../service/appraisal_functional_attributes.service';
import {ApprisalFunctionalAttribute} from '../../../model/appraisal_functional_attributes.model';
import { IMultiSelectOption ,IMultiSelectSettings,IMultiSelectTexts} from 'angular-2-dropdown-multiselect';
import {NgForm} from '@angular/forms';
import {ActivatedRoute,Router} from '@angular/router';
import {Role} from '../../../model/role.model';
import {Category} from '../../../model/goal_category.model';
import {Subcategory} from '../../../model/goal_subcategory.model';
import { NgModule } from '@angular/core';
import {HomeService} from '../../../service/home.service';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';
import { FunctionalIDService } from '../../../service/functional_id.service';
@Component({

    selector : 'apprisalFunctionalAttribute-Update',
    templateUrl: './appriasal-functional-attribute-form-update.component.html',
    providers:[ApprisalFunctionalAttributeService,HomeService,FunctionalIDService]
  
})
export class ApprisalFunctionalAttributeUpdateComponent{

    apprisalFunctionalAttribute = new ApprisalFunctionalAttribute();
    public id:String =null;
    public apprisalFunctionalAttributeResults:ApprisalFunctionalAttribute;

    functionids: String[];
    public roles:Role[];
    public categorys:Category[];
    public subcategory:Subcategory[]
    public goalSubCategoryOptions: IMultiSelectOption[];

    myOptions: IMultiSelectOption[] = [
        { id: 1, name: 'Car brands', isLabel: true },
        { id: 2, name: 'Volvo', parentId: 1 },
        { id: 3, name: 'Honda', parentId: 1 },
        { id: 4, name: 'BMW', parentId: 1 },
        { id: 5, name: 'Colors', isLabel: true },
        { id: 6, name: 'Blue', parentId: 5 },
        { id: 7, name: 'Red', parentId: 5 },
        { id: 8, name: 'White', parentId: 5 }
    ];
    
     //https://www.npmjs.com/package/angular2-dropdown-multiselect
    // Settings configuration
mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    dynamicTitleMaxItems: 2,
    displayAllSelectedText: true,
    selectionLimit :1
};
myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    searchEmptyResult: 'Nothing found...',
    searchNoRenderText: 'Type in search box to see results...',
    defaultTitle: 'Select',
    allSelected: 'All selected',
};
    constructor(private apprisalFunctionalAttributeService:ApprisalFunctionalAttributeService,
        private activatedRoute:ActivatedRoute,
        private router:Router,
        private homeService:HomeService,
        private functionalIDService: FunctionalIDService) {
            this.apprisalFunctionalAttributeResults = {
                "_id" : "",
                "function_id" : "",
                "category" : "",
                "subcategory" :"",
                "description" :"",
                "role_id" : "",
                "target_for_last_year" : "",
                "target_for_last_year_description" : "",
                "target_for_last_year_unit" : "",
                "target_for_next_year" : "",
                "target_for_next_year_description" : "",
                "target_for_next_year_unit" : "",
                "weightage" : 0
            }
        }
       

    ngOnInit() {
        try {
            this.activatedRoute.params.subscribe( params => 
                {
                    this.id=params.id;
                } );
        } catch (error) {}
        if(this.id!=null){
            var req = this.homeService.getHomeDeails();
            req.subscribe(results => {
         
                if(results!=undefined&&results["data"]!=undefined && results["data"].role!=undefined&& results["data"].role.docs!=undefined ){
                  this.roles=results["data"].role["docs"] as Role[] ;
                }
                if(results!=undefined&&results["data"]!=undefined){
                    this.categorys = results["data"].goalCategory.docs as Category[];
                    this.subcategory=results["data"].goalSubcategory.docs as Subcategory[];
                    this.goalSubCategoryOptions = [];
                    for (let entry of this.subcategory) {
                    this.goalSubCategoryOptions.push({id: ""+entry._id, name: ""+entry.name});
                    }
                  }
              });

    var res = this.apprisalFunctionalAttributeService.getApprisalFunctionalAttributeById(this.id);
    console.log(res)
    res.subscribe(results=>{
        console.log(results)
        if(results!=undefined && results["data"]!=undefined){
            this.apprisalFunctionalAttributeResults=results["data"] as ApprisalFunctionalAttribute;
            
          }
    });

   this.functionids = this.functionalIDService.getFunctionalID();
    }
    }
    onUpdate(formData){
        if(formData.goal_subcategory[0] !=undefined && formData.goal_subcategory[0] != ''){
            this.apprisalFunctionalAttribute.subcategory = formData.goal_subcategory[0];
          }
        return this.apprisalFunctionalAttributeService.updateApprisalFunctionalAttribute(this.apprisalFunctionalAttributeResults).subscribe(()=>
        {
            alert("updated");
            this.redirect();

        });
    }
    validate(){    
        if(this.apprisalFunctionalAttributeResults.target_for_last_year!=undefined && this.apprisalFunctionalAttributeResults.target_for_last_year.toString().trim().length==0){
          this.apprisalFunctionalAttributeResults.target_for_last_year="";
        }
        if(this.apprisalFunctionalAttributeResults.target_for_last_year_unit!=undefined && this.apprisalFunctionalAttributeResults.target_for_last_year_unit.toString().trim().length==0){
          this.apprisalFunctionalAttributeResults.target_for_last_year_unit="";
        }
        if(this.apprisalFunctionalAttributeResults.target_for_next_year!=undefined && this.apprisalFunctionalAttributeResults.target_for_next_year.toString().trim().length==0){
          this.apprisalFunctionalAttributeResults.target_for_next_year="";
        }
        if(this.apprisalFunctionalAttributeResults.target_for_next_year_unit!=undefined && this.apprisalFunctionalAttributeResults.target_for_next_year_unit.toString().trim().length==0){
          this.apprisalFunctionalAttributeResults.target_for_next_year_unit="";
        }
        if(this.apprisalFunctionalAttributeResults.weightage!=undefined && this.apprisalFunctionalAttributeResults.weightage.toString().trim().length==0){
          // this.apprisalFunctionalAttributeResults.weightage=0;
        }
      }
      redirect() {this.router.navigate(['/apprisal-functional-attribute-list']); }
  
}