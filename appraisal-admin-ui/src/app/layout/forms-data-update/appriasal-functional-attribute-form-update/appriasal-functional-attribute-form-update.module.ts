import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { ApprisalFunctionalAttributeUpdateRoutingModule } from './appriasal-functional-attribute-form-update-routing.module';
import { ApprisalFunctionalAttributeUpdateComponent } from './appriasal-functional-attribute-form-update.component';
import { PageHeaderModule } from './../../../shared';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';

@NgModule({
    imports: [CommonModule, ApprisalFunctionalAttributeUpdateRoutingModule, PageHeaderModule,FormsModule,MultiselectDropdownModule],
    declarations: [ApprisalFunctionalAttributeUpdateComponent]
})
export class ApprisalFunctionalAttributeUpdateModule {}
