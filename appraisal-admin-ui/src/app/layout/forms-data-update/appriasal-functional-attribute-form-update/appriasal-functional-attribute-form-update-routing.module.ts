import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApprisalFunctionalAttributeUpdateComponent } from './appriasal-functional-attribute-form-update.component';

const routes: Routes = [
    {
        path: '', component: ApprisalFunctionalAttributeUpdateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ApprisalFunctionalAttributeUpdateRoutingModule {
}