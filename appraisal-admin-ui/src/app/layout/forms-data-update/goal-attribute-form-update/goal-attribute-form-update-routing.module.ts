import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoalAttributeFormUpdateComponent } from './goal-attribute-form-update.component';

const routes: Routes = [
    {
        path: '', component: GoalAttributeFormUpdateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GoalAttributeFormUpdateRoutingModule {
}