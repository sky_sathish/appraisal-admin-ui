import { Component } from '@angular/core';
import {GoalAttributeService} from '../../../service/goal_attribute.service';
import {GoalAttribute} from '../../../model/goal_attribute.model';
import {Subcategory} from '../../../model/goal_subcategory.model';
import {NgForm} from '@angular/forms';
import {ActivatedRoute,Router} from '@angular/router';
import { Category } from '../../../model/goal_category.model';
import { HomeService } from '../../../service/home.service';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';
@Component({

    selector : 'goalAttribute-Form',
    templateUrl: './goal-attribute-form-update.component.html',
    providers:[GoalAttributeService,HomeService]
  
})
export class GoalAttributeFormUpdateComponent{
    public id:String =null;
    public goalAttributeResults:GoalAttribute;
    public categories:Category[];
    public subcategories:Subcategory[];
    constructor(private goalAttributeService:GoalAttributeService,
        private homeService:HomeService,
                private activatedRoute:ActivatedRoute,
                private router:Router) {}
    goalAttribute = new GoalAttribute();
    subcategory = new Subcategory();
    ngOnInit() {
        try {
            this.activatedRoute.params.subscribe( params => 
                {
                    this.id=params.id;
                } );
        } catch (error) {}
        if(this.id!=null){
    var goal_res1 = this.goalAttributeService.getGoalAttributeById(this.id);
    goal_res1.subscribe(results=>{
        console.log("results : "+results);
        if(results!=undefined&&results["data"]!=undefined){

            // this.goalAttributeResults=results["data"] as GoalAttribute[] ;
            this.goalAttributeResults = results["data"] as GoalAttribute;
            console.log("check goal : "+this.goalAttributeResults+" --- (attribute) "+this.goalAttributeResults.attribute);
        }
    });
    var req = this.homeService.getHomeDeails();
    req.subscribe(results => { 
      console.log( results);
        if(results!=undefined&&results["data"]!=undefined && results["data"].role!=undefined&& results["data"].role.docs!=undefined ){
          this.categories=results["data"].goalCategory.docs as Category[] ;
          this.subcategories=results["data"].goalSubcategory.docs as Subcategory[] ;
        }
      });
    }
}
    onUpdate(formData){
            return this.goalAttributeService.updateGoalAttribute(formData).subscribe(()=>
        {
            alert("updated");
            this.redirect()
        });
    }
    validate(){    
        if(this.goalAttributeResults.attribute!=undefined && this.goalAttributeResults.attribute.toString().trim().length==0){
           this.goalAttributeResults.attribute="";
        }
      }
      redirect() {this.router.navigate(['/goal-attribute-list']); }
}