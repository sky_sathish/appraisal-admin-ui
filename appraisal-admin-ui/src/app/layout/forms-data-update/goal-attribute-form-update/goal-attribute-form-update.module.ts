import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { GoalAttributeFormUpdateRoutingModule } from './goal-attribute-form-update-routing.module';
import { GoalAttributeFormUpdateComponent } from './goal-attribute-form-update.component';
import { PageHeaderModule } from './../../../shared';

@NgModule({
    imports: [CommonModule, GoalAttributeFormUpdateRoutingModule, PageHeaderModule,FormsModule],
    declarations: [GoalAttributeFormUpdateComponent]
})
export class GoalAttributeFormUpdateModule {}
