import { Component } from '@angular/core';
import {GeneralConfigService} from '../../../service/general_config.service';
import {GeneralConfig} from '../../../model/general_config.model';
import {NgForm} from '@angular/forms';
import {ActivatedRoute,Router} from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';
@Component({

    selector : 'generalConfigForm-Update',
    templateUrl: './general-config-form-update.component.html',
    providers:[GeneralConfigService]
  
})
export class GeneralConfigFormUpdateComponent{
    public id:String =null;
    public generalConfigResults:GeneralConfig;
    constructor(private generalConfigService:GeneralConfigService,
        private activatedRoute:ActivatedRoute,
        private router:Router) {}
    generalConfig = new GeneralConfig();
  
    ngOnInit() {
        try {
            this.activatedRoute.params.subscribe( params => 
                {
                    this.id=params.id;
                } );
        } catch (error) {}
      
        if(this.id!=null){
    var res = this.generalConfigService.getGeneralConfigById(this.id);
    res.subscribe(results=>{
        console.log("getGeneralConfigById results : "+results);
        if(results!=undefined&&results["data"]!=undefined){

            this.generalConfigResults=results["data"] as GeneralConfig ;
            console.log(this.generalConfigResults.key);
        }
    });
    }
    }
    onUpdate(formData){
       
        return this.generalConfigService.updateGeneralConfig(formData).subscribe(()=>
        {
            alert("updated");
        
        });
    }
}