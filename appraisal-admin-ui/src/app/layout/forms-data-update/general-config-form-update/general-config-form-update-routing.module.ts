import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralConfigFormUpdateComponent } from './general-config-form-update.component';

const routes: Routes = [
    {
        path: '', component: GeneralConfigFormUpdateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GeneralConfigFormUpdateRoutingModule {
}