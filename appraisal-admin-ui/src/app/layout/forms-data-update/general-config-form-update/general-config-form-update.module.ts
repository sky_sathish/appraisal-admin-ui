import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { GeneralConfigFormUpdateRoutingModule } from './general-config-form-update-routing.module';
import { GeneralConfigFormUpdateComponent } from './general-config-form-update.component';
import { PageHeaderModule } from './../../../shared';

@NgModule({
    imports: [CommonModule, GeneralConfigFormUpdateRoutingModule, PageHeaderModule,FormsModule],
    declarations: [GeneralConfigFormUpdateComponent]
})
export class GeneralConfigFormUpdateModule {}
