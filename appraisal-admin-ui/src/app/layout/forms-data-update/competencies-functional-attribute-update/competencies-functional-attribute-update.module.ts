import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { CompetenciesFunctionalAttributesUpdatetRoutingModule } from './competencies-functional-attribute-update-routing.module';
import { CompetenciesFunctionalAttributesUpdateComponent } from './competencies-functional-attribute-update.component';
import { PageHeaderModule } from './../../../shared';

@NgModule({
    imports: [CommonModule, CompetenciesFunctionalAttributesUpdatetRoutingModule, PageHeaderModule,FormsModule],
    declarations: [CompetenciesFunctionalAttributesUpdateComponent]
})
export class CompetenciesFunctionalAttributesUpdateModule {}
