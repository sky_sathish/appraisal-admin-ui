import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompetenciesFunctionalAttributesUpdateComponent } from './competencies-functional-attribute-update.component';

const routes: Routes = [
    {
        path: '', component: CompetenciesFunctionalAttributesUpdateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CompetenciesFunctionalAttributesUpdatetRoutingModule {
}