import { Component } from '@angular/core';
import { CompetenciesFunctionalAttributesService } from '../../../service/competencies_functional_attributes.service';
import { CompetenciesFunctionalAttributes } from '../../../model/competencies_functional_attributes.model';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HomeService } from '../../../service/home.service';
import { Role } from '../../../model/role.model';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { FunctionalIDService } from '../../../service/functional_id.service';
import { CompetenciesLevel } from '../../../model/competencies_level.model';
@Component({

    selector: 'CompetenciesFunctionalAttributes-List',
    templateUrl: './competencies-functional-attribute-update.component.html',
    providers: [CompetenciesFunctionalAttributesService, HomeService,
        FunctionalIDService]

})
export class CompetenciesFunctionalAttributesUpdateComponent {
    public id: String = null;
    public competenciesFunctionalAttributesResults = new CompetenciesFunctionalAttributes();
    functionids: String[];
    public roles: Role[];
    public competenciesLevelResults: CompetenciesLevel[];
    public competenciesFunctionalAttributes = new CompetenciesFunctionalAttributes();
    constructor(private competenciesFunctionalAttributesService: CompetenciesFunctionalAttributesService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private homeService: HomeService,
        private functionalIDService: FunctionalIDService) { 

            this.competenciesFunctionalAttributesResults = {
                "_id" : "",
                "behavioral_competencies":"",
                "competency_level" : "",
                "function_id" : "",
                "role_id" : {
                    "_id" : "",
                    "name": "Leadership and Tactical Managers "
                },
                "weightage" : 0,
                "weightage_unit" : ""
            
            }
        }
   

    ngOnInit() {
        try {
            this.activatedRoute.params.subscribe(params => {
                this.id = params.id;
            });
        } catch (error) { }
        if (this.id != null) {
            
          var res1=  this.competenciesFunctionalAttributesService.getCompetenciesFunctionalAttributesById(this.id)
                .subscribe(results => {
                    console.log("results : " + results);
                    if (results != undefined && results["data"] != undefined) {

                        this.competenciesFunctionalAttributesResults = results["data"] as CompetenciesFunctionalAttributes;
                        console.log("test --> ")
                        console.log(this.competenciesFunctionalAttributesResults)
                    }
                });
         var res2=   this.homeService.getHomeDeails()
                .subscribe(results => {
                    console.log("test homeService --> ")
                    console.log(JSON.stringify(results));
                    if (results != undefined && results["data"] != undefined && results["data"].role != undefined && results["data"].role.docs != undefined) {
                        this.roles = results["data"].role.docs as Role[];
                    }
                    if(results != undefined && results["data"] != undefined){
                        this.competenciesLevelResults = results["data"].competenciesLevel.docs as CompetenciesLevel[];
                        console.log("test competenciesLevelResults --> ")
                        console.log(JSON.stringify(this.competenciesLevelResults))
                    }
                });
     this.functionids = this.functionalIDService.getFunctionalID();
        }

    }
    onUpdate(formData) {

        return this.competenciesFunctionalAttributesService.updateCompetenciesFunctionalAttributes(formData).subscribe(() => {
            alert("updated");
            this.redirect();
        });
    }
    redirect() { this.router.navigate(['/competencies-functional-attributes-list']); }
    validate() {


        if (this.competenciesFunctionalAttributes.weightage_unit != undefined && this.competenciesFunctionalAttributes.weightage_unit.toString().trim().length == 0) {
            this.competenciesFunctionalAttributes.weightage_unit = "";
        }
    }
}