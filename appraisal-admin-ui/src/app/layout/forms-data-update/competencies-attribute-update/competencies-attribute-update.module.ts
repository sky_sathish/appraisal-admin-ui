import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { CompetenciesAttributeUpdateRoutingModule } from './competencies-attribute-update-routing.module';
import { CompetenciesAttributeUpdateComponent } from './competencies-attribute-update.component';
import { PageHeaderModule } from './../../../shared';

@NgModule({
    imports: [CommonModule, CompetenciesAttributeUpdateRoutingModule, PageHeaderModule,FormsModule],
    declarations: [CompetenciesAttributeUpdateComponent]
})
export class CompetenciesAttributeUpdateModule {}
