import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompetenciesAttributeUpdateComponent } from './competencies-attribute-update.component';

const routes: Routes = [
    {
        path: '', component: CompetenciesAttributeUpdateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CompetenciesAttributeUpdateRoutingModule {
}