import { Component } from '@angular/core';
import {CompetenciesAttributeService} from '../../../service/competencies_attributes.service';
import {CompetenciesAttribute} from '../../../model/competencies_attribute.model';
import {NgForm} from '@angular/forms';
import {ActivatedRoute,Router} from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';
@Component({

    selector : 'competenciesAttributeForm-Update',
    templateUrl: './competencies-attribute-update.component.html',
    providers:[CompetenciesAttributeService]
  
})
export class CompetenciesAttributeUpdateComponent{

    public id:String =null;
    public competenciesAttributeResults:CompetenciesAttribute;
    constructor(private competenciesAttributeService:CompetenciesAttributeService,
        private activatedRoute:ActivatedRoute,
        private router:Router) {}
    generalConfig = new CompetenciesAttribute();
  
    ngOnInit() {
        try {
            this.activatedRoute.params.subscribe( params => 
                {
                    this.id=params.id;
                } );
        } catch (error) {}
        if(this.id!=null){
    var res = this.competenciesAttributeService.getCompetenciesAttributeById(this.id);
    res.subscribe(results=>{
        console.log("results : "+results);
        if(results!=undefined&&results["data"]!=undefined){

            this.competenciesAttributeResults=results["data"] as CompetenciesAttribute;
            
        }
    });
    }
    }
    onUpdate(formData){
       
        return this.competenciesAttributeService.updateCompetenciesAttribute(formData).subscribe(()=>
        {
            alert("updated");
         
        });
    }
}