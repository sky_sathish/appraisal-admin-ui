import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            // Form path
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'table', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'appriasal-attribute-form', loadChildren: './forms/appriasal_functional_attribute_form/appriasal_fun_attr.module#AppraisalFunModule' },
            // { path: 'competencies-attribute-form', loadChildren: './forms/competencies_attribute_form/competencies_attribute.module#CompetenciesAttributeFunModule' },
            { path: 'competencies-functional-attribute-form', loadChildren: './forms/competencies_functional_attribute_form/competencies_functional_attribute_form.module#CompetenciesFunctionalAttributeFormModule' },
            { path: 'general-config-form', loadChildren: './forms/general_config_form/general_config_form.module#GeneralConfigFormModule' },
            { path: 'goal-attribute-form', loadChildren: './forms/goal-attribute-form/goal-attribute-form.module#GoalAttributeModule' },
        //    { path: 'matric-look-for-last-year-form', loadChildren: './forms/matric-look-for-last-year/matric_look_for_last_year.module#MatricLookForLastYearModule' },
             
            // form List path
            { path: 'matric-look-for-last-year-list', loadChildren: './forms-data-list/matric-look-for-last-year-list/matric-look-for-last-year-list.module#MatricLookForLastYearListModule' },
            { path: 'general-config-list', loadChildren: './forms-data-list/general-config-form-list/general-config-form-list.module#GeneralConfigFormListModule' },
            { path: 'competencies-attribute-form-list', loadChildren: './forms-data-list/competencies-attribute-form-list/competencies-attribute-form-list.module#CompetenciesAttributeFormListModule' },
            { path: 'goal-attribute-list', loadChildren: './forms-data-list/goal-attribute-form-list/goal-attribute-form-list.module#GoalAttributeFormListModule' },
            { path: 'competencies-functional-attributes-list', loadChildren: './forms-data-list/competencies-functional-attribute-list/competencies-functional-attribute-list.module#CompetenciesFunctionalAttributesListModule' },
            { path: 'apprisal-functional-attribute-list', loadChildren: './forms-data-list/appriasal-functional-attribute-form-list/appriasal-functional-attribute-form-list.module#ApprisalFunctionalAttributeListModule' },
           
            { path: 'goalcategory-list', loadChildren: './forms-data-list/goal-category-list/goal-category-list.module#GoalCategoryListModule' },
            { path: 'goalsubcategory-list', loadChildren: './forms-data-list/goal-subcategory-list/goal-subcategory-list.module#GoalSubCategoryListModule' },
            { path: 'role-list', loadChildren: './forms-data-list/role-list/role-list.module#RoleListModule' },
            { path: 'competencies-level', loadChildren: './forms-data-list/competencies-level-list/competencies-level-list.module#CompetenciesLevelListModule' },
            { path: 'appraisal', loadChildren: './forms-data-list/appraisal-master/appraisal-master-list.module#AppraisalMasterListModule' },
            { path: 'functional-list', loadChildren: './forms-data-list/functional_attribute/functional_attribute.module#FunctionalListModule' },


            // new
            { path: 'review-of-goals-list', loadChildren: './forms-data-list/review-of-goals/review-of-goal.module#ReviewOFGoalsListModule' },
            { path: 'review-of-competencies-list', loadChildren: './forms-data-list/review-of-competencies/review-of-competencies.module#ReviewOFCompetenciesListModule' },

            // Form Update path
       //     { path: 'matriclockform-update/:id', loadChildren: './forms-data-update/matric-look-for-last-year-update/matric-look-for-last-year-update.module#MatricLookForLastYearUpdateModule' },
            { path: 'general-config-form/:id', loadChildren: './forms-data-update/general-config-form-update/general-config-form-update.module#GeneralConfigFormUpdateModule' },
            // { path: 'competencies-attribute-form/:id', loadChildren: './forms-data-update/competencies-attribute-update/competencies-attribute-update.module#CompetenciesAttributeUpdateModule' },
            { path: 'goal-attribute-form/:id', loadChildren: './forms-data-update/goal-attribute-form-update/goal-attribute-form-update.module#GoalAttributeFormUpdateModule' },
            { path: 'apprisal-functional-attribute-form/:id', loadChildren: './forms-data-update/appriasal-functional-attribute-form-update/appriasal-functional-attribute-form-update.module#ApprisalFunctionalAttributeUpdateModule' },
            { path: 'competencies-functional-attributes-form/:id', loadChildren: './forms-data-update/competencies-functional-attribute-update/competencies-functional-attribute-update.module#CompetenciesFunctionalAttributesUpdateModule' }

                ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
