import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { PageHeaderModule } from './../../../shared';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { ReviewOfGoalsListComponent } from './review-of-goal.components'
import {GoalActionsComponent} from './review-of-goal-actions/review-of-goal-actions.component'
import {ReviewOfGoalsListRoutingModule} from './review-of-goal-routing.module'
@NgModule({
    imports: [
        CommonModule, 
        PageHeaderModule,
        FormsModule,
        ReviewOfGoalsListRoutingModule,
        MultiselectDropdownModule,
        NgbModule.forRoot()
    ],
    declarations: [ReviewOfGoalsListComponent,GoalActionsComponent]
})
export class ReviewOFGoalsListModule {}
