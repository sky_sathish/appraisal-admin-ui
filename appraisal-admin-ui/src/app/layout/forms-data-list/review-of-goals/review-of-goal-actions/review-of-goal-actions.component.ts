import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ReviewOfGoalsModel } from './../../../../model/review_of_goals.model'
import { IMultiSelectOption, IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import { FunctionalIDService } from '../../../../service/functional_id.service';
import { Role } from '../../../../model/role.model';
import { Category } from '../../../../model/goal_category.model';
import { Subcategory } from '../../../../model/goal_subcategory.model';
import { ReviewOfGoalsTargetModel } from '../../../../model/review_of_goals_target.model';
import { HomeService } from '../../../../service/home.service';
import { ReviewOfGoalsService } from '../../../../service/review_of_goals.service';
import { ReviewOfGoalsTargetService } from '../../../../service/review_of_goals_target.service';
@Component({

    selector: 'goals-action',
    templateUrl: './review-of-goal-actions.component.html',
    providers: [FunctionalIDService, HomeService, ReviewOfGoalsService, ReviewOfGoalsTargetService],
    animations: []

})

export class GoalActionsComponent {
    id: String = null;
    isSaveAction: boolean = true;
    functions: String[];
    roles: Role[];
    categorys: Category[];
    subcategory: Subcategory[]
    roleOptions: IMultiSelectOption[];
    subCategoryOptions: IMultiSelectOption[];
    reviewOfGoalsModel = new ReviewOfGoalsModel();
    reviewOfGoalsTargetModel = new ReviewOfGoalsTargetModel();
    reviewOfGoalsResults = new ReviewOfGoalsModel;
    reviewOfGoalsTargetResults: ReviewOfGoalsTargetModel[];
    reviewOfGoalsTargetObject = new ReviewOfGoalsTargetModel();
    closeResult: string;
    saveOrUpdateModelBtnValue: string = 'Save';
    loading: boolean = false;


    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private functionalIDService: FunctionalIDService,
        private homeService: HomeService,
        private reviewOfGoalsService: ReviewOfGoalsService,
        private reviewOfGoalsTargetService: ReviewOfGoalsTargetService,
        private modalService: NgbModal
    ) {
        this.reviewOfGoalsResults = {
            "_id": "",
            "category": {
                "_id": "",
                "name": ""
            },
            "description": "",
            "function": "",
            "role": {
                "_id": "",
                "name": ""
            },
            "subcategory": {
                "_id": "",
                "name": ""
            }
        }

    }
    //https://www.npmjs.com/package/angular2-dropdown-multiselect
    mySettings: IMultiSelectSettings = {
        enableSearch: true,
        checkedStyle: 'fontawesome',
        dynamicTitleMaxItems: 1,
        displayAllSelectedText: true,
        selectionLimit: 1
    };

    ngOnInit() {

        // common data
        //functions call
        this.functions = this.functionalIDService.getFunctionalID();

        //homeService call (roles,category and subcategory)
        this.homeService.getHomeDeails().subscribe(results => {

            // category and subcategory
            if (results != undefined && results["data"] != undefined && results["data"] != undefined) {
                this.categorys = results["data"].goalCategory.docs as Category[];
                this.subcategory = results["data"].goalSubcategory.docs as Subcategory[];

                this.subCategoryOptions = [];
                for (let entry of this.subcategory) {
                    this.subCategoryOptions.push({ id: "" + entry._id, name: "" + entry.name });
                }
            }
            // roles
            if (results != undefined && results["data"] != undefined && results["data"].role != undefined && results["data"].role.docs != undefined) {
                this.roles = results["data"].role.docs as Role[];
                this.roleOptions = [];
                for (let entry of this.roles) {
                    this.roleOptions.push({ id: "" + entry._id, name: "" + entry.name });
                }
            }
        });

        // Routing
        try {
            this.activatedRoute.params.subscribe(params => {
                this.id = params.id;
            });
        } catch (error) { }
        if (this.id != null) {
            this.isSaveAction = false;
            this.reviewOfGoalsService.getReviewOfGoalsById(this.id).subscribe((result) => {
                if (result != undefined && result["data"] != undefined && result["data"]["reviewOfGoals"] != undefined && result["data"]["reviewOfGoalsTarget"] != undefined)
                    this.reviewOfGoalsResults = result["data"]["reviewOfGoals"] as ReviewOfGoalsModel;
                this.reviewOfGoalsTargetResults = result["data"]["reviewOfGoalsTarget"] as ReviewOfGoalsTargetModel[];
            })
        }

    }
    //Review of Goals

    //custome validate
    validate() {

    }

    onSave() {
        var data: any;
        // bind 2 models
        data = {
            "reviewOfGoals": this.reviewOfGoalsModel,
            "reviewOfGoalsTarget": this.reviewOfGoalsTargetModel
        }
        this.reviewOfGoalsService.addReviewOfGoals(data).subscribe((res) => {
            alert(JSON.stringify(res.message))
            this.redirect("form")
        })
    }
    onUpdateReviewOfGoals() {
        this.loading = true;
        this.reviewOfGoalsService.updateReviewOfGoals(this.reviewOfGoalsResults).subscribe((res) => {
            alert(JSON.stringify(res.message))
            this.loading = false;
            this.redirect("form-id")
        })
    }
    onModelOpen(content, reviewOfGoalsTarget, action) {
        if (action == 'edit') {
            this.saveOrUpdateModelBtnValue = "Update";
            this.reviewOfGoalsTargetObject = {
                "_id": reviewOfGoalsTarget._id,
                "review_goals_id": reviewOfGoalsTarget.review_goals_id,
                "target": reviewOfGoalsTarget.target,
                "weightage": reviewOfGoalsTarget.weightage,
                "year": reviewOfGoalsTarget.year,
                "term": reviewOfGoalsTarget.term,
                "active_status": reviewOfGoalsTarget.active_status
            }
        } else {
            this.saveOrUpdateModelBtnValue = "Save";
            this.reviewOfGoalsTargetObject = {
                "_id": "",
                "review_goals_id": this.id,
                "target": "",
                "weightage": "",
                "year": "",
                "term": "",
                "active_status": true
            }
        }
        this.modalService.open(content).result.then(
            (result) => { },
            (reason) => { }
        ).catch();
    }
    onSaveOrUpdateModel(action) {
        if (action == 'Save') {
            this.loading = true;
            this.reviewOfGoalsTargetService.addReviewOfGoalsTarget(this.reviewOfGoalsTargetObject)
                .subscribe((result) => {
                    alert(result.message)
                    this.loading = false;
                })
        } else {
            this.loading = true;
            this.reviewOfGoalsTargetService.updateReviewOfGoalsTarget(this.reviewOfGoalsTargetObject)
                .subscribe((result) => {
                    alert(result.message)
                    this.loading = false;
                })
        }
        this.redirect("form-id")

    }
    onDelete(data) {
        if (confirm("Are you sure to delete?")) {
            this.loading = true;
            this.reviewOfGoalsTargetService.deleteReviewOfGoalsTarget(data._id).then(() => {
                var index = this.reviewOfGoalsTargetResults.indexOf(data, 0);
                if (index > -1) {
                    this.reviewOfGoalsTargetResults.splice(index, 1);
                }
                this.loading = false;
            })
        }
    }
    redirect(action) {
        if (action == 'form-id') {
            window.location.reload();
        } else {
            this.router.navigate(['/review-of-goals-list']);
        }

    }


}