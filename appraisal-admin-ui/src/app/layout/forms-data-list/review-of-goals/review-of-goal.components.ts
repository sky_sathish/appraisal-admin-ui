import { Component } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import {CloneModel} from './../../../model/clone.model'
import {ReviewOfGoalsModel} from './../../../model/review_of_goals.model'
import {ReviewOfGoalsService} from '../../../service/review_of_goals.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
@Component({

    selector: 'functional-List',
    templateUrl: './review-of-goal.components.html',
    providers: [ReviewOfGoalsService],
    animations: [routerTransition()]

})
export class ReviewOfGoalsListComponent {

    reviewOfGoalsResults:ReviewOfGoalsModel[]
    cloneModel = new CloneModel();
    loading:boolean =false;
    constructor(private reviewOfGoalsService:ReviewOfGoalsService,
        private modalService: NgbModal) {
            this.cloneModel ={
                "fromYear":"",
                "fromTerm":"",
                "toYear":"",
                "toTerm":""
            }
    }


    ngOnInit() {
        this.loading = true;
        this.reviewOfGoalsService.getAllReviewOfGoals().then((result)=>{

            if(result!=undefined && result["data"]!=undefined){
                this.reviewOfGoalsResults = result["data"] as ReviewOfGoalsModel[];
                this.loading = false;
            }
        })

    }

    onDelete(data){
        if(confirm("Are you sure to delete?")) {
        this.loading = true;
        this.reviewOfGoalsService.deleteReviewOfGoals(data._id).then(()=>{
           
                var index = this.reviewOfGoalsResults.indexOf(data, 0);
                if (index > -1)
                {
                    this.reviewOfGoalsResults.splice(index, 1);
                }
                this.loading = false;
        }) 
    }
}

// clone section
onModelOpenReviewOfGoalsClone(ReviewOfGoalsClone){
    this.modalService.open(ReviewOfGoalsClone)
    .result.then(
        (result) => {
            
        }, 
        (reason) => {}
    ).catch();
}
onCloneReviewOfGoals(){
    this.loading = true;
    this.reviewOfGoalsService.cloneReviewOfGoals(this.cloneModel).subscribe((res)=>{
        alert(JSON.stringify(res.message))
        this.loading = false;
    })

}
// clone section end
  
  
 
}