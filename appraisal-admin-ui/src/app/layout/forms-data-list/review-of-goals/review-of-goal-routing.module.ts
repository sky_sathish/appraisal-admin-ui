import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReviewOfGoalsListComponent } from './review-of-goal.components'
import {GoalActionsComponent} from './review-of-goal-actions/review-of-goal-actions.component'
const routes: Routes = [
    {
         path: '', component: ReviewOfGoalsListComponent
     } ,
     {
         path: 'form', component: GoalActionsComponent
     },
     {
         path: 'form/:id', component: GoalActionsComponent
     }
 ];
 
 @NgModule({
     imports: [RouterModule.forChild(routes)],
     exports: [RouterModule]
 })
 export class ReviewOfGoalsListRoutingModule {
 }
 