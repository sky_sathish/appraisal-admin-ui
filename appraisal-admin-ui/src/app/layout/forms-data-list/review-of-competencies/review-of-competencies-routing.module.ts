import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReviewOfCompetenciesListComponent } from './review-of-competencies.components'
import {CompetenciesLevelActionsComponent} from './review-of-competencies-actions/review-of-competencies-actions.components'
const routes: Routes = [
    {
         path: '', component: ReviewOfCompetenciesListComponent
     } ,
     {
         path: 'form', component: CompetenciesLevelActionsComponent
     },
     {
         path: 'form/:id', component: CompetenciesLevelActionsComponent
     }
 ];
 
 @NgModule({
     imports: [RouterModule.forChild(routes)],
     exports: [RouterModule]
 })
 export class ReviewOfCompetenciesListRoutingModule {
 }
 