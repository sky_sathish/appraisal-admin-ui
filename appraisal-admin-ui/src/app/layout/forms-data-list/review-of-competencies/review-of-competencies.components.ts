import { Component } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { CloneModel } from './../../../model/clone.model'
import { ReviewOfCompetenciesModel } from './../../../model/review_of_competencies.model'
import { ReviewOfCompetenciesService } from '../../../service/review_of_competencies.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
@Component({

    selector: 'functional-List',
    templateUrl: './review-of-competencies.components.html',
    providers: [ReviewOfCompetenciesService],
    animations: [routerTransition()]

})
export class ReviewOfCompetenciesListComponent {

    reviewOfCompetenciesResults: ReviewOfCompetenciesModel[]
    cloneModel = new CloneModel();
    loading: boolean = false;

    constructor(private reviewOfCompetenciesService: ReviewOfCompetenciesService,
        private modalService: NgbModal) {
        this.cloneModel = {
            "fromYear": "",
            "fromTerm": "",
            "toYear": "",
            "toTerm": ""
        }
    }


    ngOnInit() {
        this.loading = true;
        this.reviewOfCompetenciesService.getAllReviewOfCompetencies().then((result) => {

            if (result != undefined && result["data"] != undefined) {
                this.reviewOfCompetenciesResults = result["data"] as ReviewOfCompetenciesModel[];
            }
            this.loading = false;
        })

    }

    onDelete(data) {
        if (confirm("Are you sure to delete?")) {
            this.loading = true;
            this.reviewOfCompetenciesService.deleteReviewOfCompetencies(data._id).then(() => {

                var index = this.reviewOfCompetenciesResults.indexOf(data, 0);
                if (index > -1) {
                    this.reviewOfCompetenciesResults.splice(index, 1);
                }
                this.loading = false;
            })
        }
    }

    // clone section
    onModelOpenReviewOfCompetenciesClone(ReviewOfCompetenciesClone) {
        this.modalService.open(ReviewOfCompetenciesClone)
            .result.then(
                (result) => { },
                (reason) => { }
            ).catch();
    }
    onCloneReviewOfCompetencies() {
        this.loading = true;
        this.reviewOfCompetenciesService.cloneReviewOfCompetencies(this.cloneModel).subscribe((res) => {
            alert(JSON.stringify(res.message))
            this.loading = false;
        })

    }
    // clone section end


}