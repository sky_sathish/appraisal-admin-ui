import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { PageHeaderModule } from './../../../shared';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { ReviewOfCompetenciesListComponent } from './review-of-competencies.components'
import {CompetenciesLevelActionsComponent} from './review-of-competencies-actions/review-of-competencies-actions.components'
import {ReviewOfCompetenciesListRoutingModule} from './review-of-competencies-routing.module'
@NgModule({
    imports: [
        CommonModule, 
        PageHeaderModule,
        FormsModule,
        ReviewOfCompetenciesListRoutingModule,
        MultiselectDropdownModule,
        NgbModule.forRoot()
    ],
    declarations: [ReviewOfCompetenciesListComponent,CompetenciesLevelActionsComponent]
})
export class ReviewOFCompetenciesListModule {}
