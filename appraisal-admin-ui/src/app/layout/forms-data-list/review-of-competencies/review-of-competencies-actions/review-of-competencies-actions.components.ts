import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { IMultiSelectOption, IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import { FunctionalIDService } from '../../../../service/functional_id.service';
import { Role } from '../../../../model/role.model';
import { ReviewOfCompetenciesLevelModel } from '../../../../model/review_of_competencies_level.model';
import { ReviewOfCompetenciesModel } from './../../../../model/review_of_competencies.model'
import { CompetenciesLevel } from '../../../../model/competencies_level.model';
import { HomeService } from '../../../../service/home.service';
import { ReviewOfCompetenciesService } from '../../../../service/review_of_competencies.service';
import { ReviewOfCompetenciesLevelService } from '../../../../service/review_of_competencies_level.service';
@Component({

    selector: 'goals-action',
    templateUrl: './review-of-competencies-actions.components.html',
    providers: [FunctionalIDService, HomeService, ReviewOfCompetenciesService, ReviewOfCompetenciesLevelService],
    animations: []

})

export class CompetenciesLevelActionsComponent {
    id: String = null;
    isSaveAction: boolean = true;
    functions: String[];
    roles: Role[];
    roleOptions: IMultiSelectOption[];
    competenciesLevelResults: CompetenciesLevel[];
    reviewOfCompetenciesModel = new ReviewOfCompetenciesModel();
    reviewOfCompetenciesLevelModel = new ReviewOfCompetenciesLevelModel();
    reviewOfCompetenciesResults = new ReviewOfCompetenciesModel;
    reviewOfCompetenciesLevelResults: ReviewOfCompetenciesLevelModel[];
    reviewOfCompetenciesLevelObject = new ReviewOfCompetenciesLevelModel();
    closeResult: string;
    saveOrUpdateModelBtnValue: string = 'Save';
    loading: boolean = false;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private functionalIDService: FunctionalIDService,
        private homeService: HomeService,
        private reviewOfCompetenciesService: ReviewOfCompetenciesService,
        private reviewOfCompetenciesLevelService: ReviewOfCompetenciesLevelService,
        private modalService: NgbModal
    ) {
        this.reviewOfCompetenciesResults = {
            "_id": "",
            "description": "",
            "function": "",
            "role": {
                "_id": "",
                "name": ""
            }

        }

    }
    //https://www.npmjs.com/package/angular2-dropdown-multiselect
    mySettings: IMultiSelectSettings = {
        enableSearch: true,
        checkedStyle: 'fontawesome',
        dynamicTitleMaxItems: 1,
        displayAllSelectedText: true,
        selectionLimit: 1
    };

    ngOnInit() {
        this.loading = true;
        // common data
        //functions call
        this.functions = this.functionalIDService.getFunctionalID();
        //homeService call (roles,category and subcategory)
        this.homeService.getHomeDeails().subscribe(results => {

            // competencies Level
            this.competenciesLevelResults = results["data"].competenciesLevel.docs as CompetenciesLevel[];

            // roles
            if (results != undefined && results["data"] != undefined && results["data"].role != undefined && results["data"].role.docs != undefined) {
                this.roles = results["data"].role.docs as Role[];
                this.roleOptions = [];
                for (let entry of this.roles) {
                    this.roleOptions.push({ id: "" + entry._id, name: "" + entry.name });
                }
            }
            this.loading = false;
        });

        // Routing
        try {
            this.activatedRoute.params.subscribe(params => {
                this.id = params.id;
            });
        } catch (error) { }
        if (this.id != null) {
            this.isSaveAction = false;
            this.loading = true;
            this.reviewOfCompetenciesService.getReviewOfCompetenciesById(this.id).subscribe((result) => {

                if (result != undefined && result["data"] != undefined && result["data"]["reviewOfCompetencies"] != undefined && result["data"]["reviewOfCompetenciesLevel"] != undefined) {
                    this.reviewOfCompetenciesResults = result["data"]["reviewOfCompetencies"] as ReviewOfCompetenciesModel;
                    this.reviewOfCompetenciesLevelResults = result["data"]["reviewOfCompetenciesLevel"] as ReviewOfCompetenciesLevelModel[];
                }
                this.loading = false;
            })
        }

    }
    //Review of Goals

    //custome validate
    validate() {

    }

    onSave() {
        var data: any;
        // bind 2 models
        data = {
            "reviewOfCompetencies": this.reviewOfCompetenciesModel,
            "reviewOfCompetenciesLevel": this.reviewOfCompetenciesLevelModel
        }
        this.loading = true;
        this.reviewOfCompetenciesService.addReviewOfCompetencies(data).subscribe((res) => {
            alert(JSON.stringify(res.message))
            this.loading = false;
            this.redirect("form")
        })
    }
    onUpdateReviewOfCompetencies() {
        this.loading = true;
        this.reviewOfCompetenciesService.updateReviewOfCompetencies(this.reviewOfCompetenciesResults).subscribe((res) => {
            alert(JSON.stringify(res.message))
            this.loading = false;
            this.redirect("form-id")
        })
    }
    onModelOpen(content, reviewOfCompetenciesLevel, action) {
        if (action == 'edit') {
            this.saveOrUpdateModelBtnValue = "Update";
            this.reviewOfCompetenciesLevelObject = {
                "_id": reviewOfCompetenciesLevel._id,
                "review_competencies_id": reviewOfCompetenciesLevel.review_competencies_id,
                "competencies_level": reviewOfCompetenciesLevel._id,
                "weightage": reviewOfCompetenciesLevel.weightage,
                "year": reviewOfCompetenciesLevel.year,
                "term": reviewOfCompetenciesLevel.term,
                "active_status": reviewOfCompetenciesLevel.active_status
            }
        } else {
            this.saveOrUpdateModelBtnValue = "Save";
            this.reviewOfCompetenciesLevelObject = {
                "_id": "",
                "review_competencies_id": this.id,
                "competencies_level": {
                    "_id": "",
                    "name": ""
                },
                "weightage": "",
                "year": "",
                "term": "",
                "active_status": true
            }
        }
        this.modalService.open(content).result.then(
            (result) => { },
            (reason) => { }
        ).catch();
    }
    onSaveOrUpdateModel(action) {
        if (action == 'Save') {
            this.loading = true;
            this.reviewOfCompetenciesLevelService.addReviewOfCompetenciesLevel(this.reviewOfCompetenciesLevelObject)
                .subscribe((result) => {
                    alert(result.message)
                    this.loading = false;
                })
        } else {
            this.loading = true;
            this.reviewOfCompetenciesLevelService.updateReviewOfCompetenciesLevel(this.reviewOfCompetenciesLevelObject)
                .subscribe((result) => {
                    alert(result.message)
                    this.loading = false;
                })
        }
        this.redirect("form-id")

    }
    onDelete(data) {
        if (confirm("Are you sure to delete?")) {
            this.loading = true;
            this.reviewOfCompetenciesLevelService.deleteReviewOfCompetenciesLevel(data._id).then(() => {
                var index = this.reviewOfCompetenciesLevelResults.indexOf(data, 0);
                if (index > -1) {
                    this.reviewOfCompetenciesLevelResults.splice(index, 1);
                }
                this.loading = false;
            })
        }
    }
    redirect(action) {
        if (action == 'form-id') {
            window.location.reload();
        } else {
            this.router.navigate(['/review-of-competencies-list']);
        }

    }


}