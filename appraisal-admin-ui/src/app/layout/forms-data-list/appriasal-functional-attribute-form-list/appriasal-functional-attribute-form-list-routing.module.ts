import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApprisalFunctionalAttributeListComponent } from './appriasal-functional-attribute-form-list.component';

const routes: Routes = [
    {
        path: '', component: ApprisalFunctionalAttributeListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ApprisalFunctionalAttributeListRoutingModule {
}