import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { ApprisalFunctionalAttributeListRoutingModule } from './appriasal-functional-attribute-form-list-routing.module';
import { ApprisalFunctionalAttributeListComponent } from './appriasal-functional-attribute-form-list.component';
import { PageHeaderModule } from './../../../shared';
import { Ng2SmartTableModule } from 'ng2-smart-table';
@NgModule({
    imports: [CommonModule, ApprisalFunctionalAttributeListRoutingModule, PageHeaderModule,FormsModule,Ng2SmartTableModule],
    declarations: [ApprisalFunctionalAttributeListComponent]
})
export class ApprisalFunctionalAttributeListModule {}
