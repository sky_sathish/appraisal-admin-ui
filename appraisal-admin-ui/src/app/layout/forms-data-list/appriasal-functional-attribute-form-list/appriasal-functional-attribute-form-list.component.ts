import { Component } from '@angular/core';
import {ApprisalFunctionalAttributeService} from '../../../service/appraisal_functional_attributes.service';
import {ApprisalFunctionalAttribute} from '../../../model/appraisal_functional_attributes.model';
import {GoalAttribute} from '../../../model/goal_attribute.model';
import {MatrixLookForLastYear} from '../../../model/matric_look_for_last_year.model';
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { routerTransition } from '../../../router.animations';
import { LocalDataSource } from 'ng2-smart-table';
@Component({

    selector : 'apprisalFunctionalAttribute-List',
    templateUrl: './appriasal-functional-attribute-form-list.component.html',
    providers:[ApprisalFunctionalAttributeService],
    animations: [routerTransition()]
  
})
export class ApprisalFunctionalAttributeListComponent{

    public apprisalFunctionalAttributeResults:ApprisalFunctionalAttribute[];
    matrixLookForLastYear = new MatrixLookForLastYear();
    goalAttribute= new GoalAttribute();

    public goalAttributeOptions: IMultiSelectOption[];
    public matrixToLookForOptions: IMultiSelectOption[];

    public matrixsLookForLastYear:MatrixLookForLastYear[]
    public goalAttributes:GoalAttribute[]

    switchOnOff = true;

    perPageRecord = 5;
    source: LocalDataSource;

     // smart table (documentaion link:https://akveo.github.io/ng2-smart-table/#/documentation)
     settings = {
        columns: {
            seq: {
                title: 'No.',
                editable: false,
                width: '10%'
            },
            function_id: {
                title: 'Function Id',
                editable: false,
                width: '10%'
            },
            role: {
                title: 'Role',
                editable: false,
                width: '10%'
            },
            goal_attributes: {
                title: 'Goal Attributes',
                editable: false,
                width: '10%'
            },
            metrics_to_look_for: {
                title: 'Metrics To Look For',
                editable: false,
                width: '10%'
            },
            target_for_last_year: {
                title: 'Target For Last Year',
                editable: false,
                width: '10%'
            },
            target_for_last_year_description: {
                title: 'Target For Last Year Description',
                editable: false,
                width: '10%'
            },
            target_for_next_year: {
                title: 'Target For Next Year',
                editable: false,
                width: '10%'
            },
            target_for_next_year_description: {
                title: 'Target For Next Year Description',
                editable: false,
                width: '10%'
            },
            weightage: {
                title: 'Weightage',
                editable: false,
                width: '10%'
            }
        },
        mode: 'external',//'inline'(can edit user-side) | 'external'(can not be edit user-side)
        hideHeader: false,
        hideSubHeader: false,// eg.search field
        noDataMessage: "Data not found!!!",
        actions: {
            add: false,
            edit: false,
            delete: false,
            custom: [
                { name: 'editCall', title: '<i class="fa fa-edit"></i>&nbsp;' },
                { name: 'deleteCall', title: '<i class="fa fa-trash"></i>&nbsp;' }
            ]

        },
        pager: {
            display: true,
            perPage: this.perPageRecord
        }
    };
    constructor(private apprisalFunctionalAttributeService:ApprisalFunctionalAttributeService) {
    }
   
    ngOnInit() {
    
    var res = this.apprisalFunctionalAttributeService.getApprisalFunctionalAttribute();
    res.subscribe(results=>{
        if(results!=undefined&&results["data"]!=undefined){
            this.apprisalFunctionalAttributeResults=results["data"] as ApprisalFunctionalAttribute[];
        }
    });
    }
    switchTable(){
        if(this.switchOnOff == true){
            this.switchOnOff = false;
        }else{
        this.switchOnOff = true;
        }
    }
    onCustomAction(event) {

        if (event["action"] != undefined) {
            var id = null;
            id = event["data"]["id"];

            if (event["action"] == "editCall" && id !=null && id !=undefined) {
                window.location.href = "/apprisal-functional-attribute-form/" + id;
            }
            if (event["action"] == "deleteCall" && id !=null && id !=undefined) {
                if (confirm("Are you sure to delete?")) {
                    return this.apprisalFunctionalAttributeService.deleteApprisalFunctionalAttributer(id).then(result => {
                        this.source.remove(event["data"]).then(ele=>{
                         });
                    });
                }

            }
        }
    }
    onDelete(data){
        if(confirm("Are you sure to delete?")) {
        return this.apprisalFunctionalAttributeService.deleteApprisalFunctionalAttributer(data._id).then(()=>
        {
            var index = this.apprisalFunctionalAttributeResults.indexOf(data, 0);
            if (index > -1)
            {
                this.apprisalFunctionalAttributeResults.splice(index, 1);
            }
         
        });
    }
    }
}