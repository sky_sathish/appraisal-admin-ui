import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FunctionalListComponent } from './functional_attribute.component';
import { FunctionsAddORUpdateComponent } from './functions_add_and_update/functions_add_and_update.component';
const routes: Routes = [
   {
        path: '', component: FunctionalListComponent
    } ,
    {
        path: 'form', component: FunctionsAddORUpdateComponent
    },
    {
        path: 'form/:id', component: FunctionsAddORUpdateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FunctionalListRoutingModule {
}
