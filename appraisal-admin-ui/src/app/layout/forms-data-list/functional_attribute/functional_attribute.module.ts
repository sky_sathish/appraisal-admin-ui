import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { PageHeaderModule } from './../../../shared';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FunctionalListComponent} from './functional_attribute.component'
import { FunctionsAddORUpdateComponent } from './functions_add_and_update/functions_add_and_update.component';
import {FunctionalListRoutingModule} from './functional_attribute_routing.module'
@NgModule({
    imports: [
        CommonModule, 
        PageHeaderModule,
        FormsModule,
        Ng2SmartTableModule,
        FunctionalListRoutingModule
    ],
    declarations: [FunctionalListComponent,FunctionsAddORUpdateComponent]
})
export class FunctionalListModule {}
