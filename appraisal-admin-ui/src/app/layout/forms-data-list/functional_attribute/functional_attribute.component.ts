import { Component } from '@angular/core';
import { FunctionalList } from '../../../model/functional_list.model';
import { FunctionalIDService } from '../../../service/functional_id.service';
import { routerTransition } from '../../../router.animations';
import { LocalDataSource } from 'ng2-smart-table';
@Component({

    selector: 'functional-List',
    templateUrl: './functional_attribute.component.html',
    providers: [FunctionalIDService],
    animations: [routerTransition()]

})
export class FunctionalListComponent {

    public functionalListResults: FunctionalList[];
    switchOnOff = false;
    perPageRecord = 5;
    source: LocalDataSource;
    loading: boolean = false;

    constructor(private functionalIdService: FunctionalIDService) {
        this.source = new LocalDataSource();
        functionalIdService.getFunctionalList().then(result => {
            this.onExtract(result);
        });

    }

    // smart table (documentaion link:https://akveo.github.io/ng2-smart-table/#/documentation)
    settings = {
        columns: {
            seq: {
                title: 'No.',
                editable: false,
                width: '10%'
            },
            name: {
                title: 'Function Name',
                editable: false,
                width: '90%'
            }
        },
        mode: 'external',//'inline'(can edit user-side) | 'external'(can not be edit user-side)
        hideHeader: false,
        hideSubHeader: false,// eg.search field
        noDataMessage: "Data not found!!!",
        actions: {
            add: false,
            edit: false,
            delete: false,
            custom: [
                { name: 'editCall', title: '<i class="fa fa-edit"></i>&nbsp;' },
                { name: 'deleteCall', title: '<i class="fa fa-trash"></i>&nbsp;' }
            ],

        },
        pager: {
            display: true,
            perPage: this.perPageRecord
        }
    };

    ngOnInit() {

        this.loading = true;
        this.functionalIdService.getFunctionalList()
            .then(result => {
                if (result != undefined && result["data"] != undefined && result["data"]["docs"] != undefined) {
                    this.functionalListResults = result["data"]["docs"] as FunctionalList[];
                }
                this.loading = false;
            })
            .catch();
    }


    onCustomAction(event) {

        if (event["action"] != undefined) {
            var id = null;
            id = event["data"]["id"];

            if (event["action"] == "editCall" && id != null && id != undefined) {
                window.location.href = "/functional-list/form/" + id;
            }
            if (event["action"] == "deleteCall" && id != null && id != undefined) {
                if (confirm("Are you sure to delete?")) {
                    return this.functionalIdService.deleteFunctions(id).then(result => {
                        this.source.remove(event["data"]).then(ele => {
                        });
                    });
                }

            }
        }
    }
    onExtract(result) {
        if (result != undefined && result["data"] != undefined && result["data"]["docs"] != undefined) {
            var data = [];
            var originalData = [];
            data = result["data"]["docs"];
            var count = 0;
            for (var i = 0; i < data.length; i++) {
                count = count + 1;
                originalData.push(
                    {
                        "seq": count,
                        "name": data[i].name,
                        "id": data[i]._id
                    }
                );
            }
            this.source.load(originalData);
        }
    }
    switchTable() {
        if (this.switchOnOff == true) {
            this.switchOnOff = false;
        } else {
            this.switchOnOff = true;
        }
    }
    autoAddFunctionsList() {
        this.loading = true;
        return this.functionalIdService.autoAddFunctions({}).subscribe((result) => {
            alert("Functions successfully added.")
            this.loading = false;
        })

    }
    onDelete(data) {
        if (confirm("Are you sure to delete?")) {
            this.loading = true;
            return this.functionalIdService.deleteFunctions(data._id).then(() => {
                var index = this.functionalListResults.indexOf(data, 0);
                if (index > -1) {
                    this.functionalListResults.splice(index, 1);
                }
                alert("deleted")
                this.loading = false;
            });

        }
    }
}