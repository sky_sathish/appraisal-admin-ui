import { Component } from '@angular/core';
import { FunctionalList } from '../../../../model/functional_list.model';
import { FunctionalIDService } from '../../../../service/functional_id.service';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { routerTransition } from '../../../../router.animations';
@Component({

    selector: 'functions-List',
    templateUrl: './functions_add_and_update.component.html',
    providers: [FunctionalIDService],
    animations: [routerTransition()]

})
export class FunctionsAddORUpdateComponent {

    isSave: boolean = true;
    isUpdate: boolean = true;
    isValid: boolean = true;
    isValidErrorMsg: string = "";
    isDuplicate: boolean = false;
    buttonValue: String = "Save";
    id: String = null;
    functionalListResults: FunctionalList;
    functionalList = new FunctionalList();
    loading: boolean = false;

    constructor(private activatedRoute: ActivatedRoute,
        private router: Router,
        private functionalServiceService: FunctionalIDService) {
        this.functionalListResults = {
            "_id": "",
            "name": ""
        }
    }

    ngOnInit() {
        try {
            this.activatedRoute.params.subscribe(params => {
                this.id = params.id;
            });
        } catch (error) { }
        if (this.id != null) {
            this.isUpdate = true;
            this.isSave = false;
            this.loading = true;
            var res = this.functionalServiceService.getFunctionsById(this.id);
            res.subscribe(results => {
                console.log("results : " + results);
                if (results != undefined && results["data"] != undefined) {

                    this.functionalListResults = results["data"] as FunctionalList;

                }
                this.loading = false;
            });
        } else {
            this.isUpdate = false;
            this.isSave = true;
        }
        if ((this.isSave == true) && (this.isUpdate == false)) {
            this.buttonValue = "Save";
        }
        if ((this.isSave == false) && (this.isUpdate == true)) {
            this.buttonValue = "Update";
        }
    }

    onAddORUpdate(formData) {
        if (this.isSave == true) {
            this.loading = true;
            return this.functionalServiceService.addFunctions(formData).subscribe((data) => {
                if (data != undefined && data.success != undefined) {
                    if (data.success == true) {
                        alert("Added");
                        this.isValid = true;
                        this.redirect();
                    } else {
                        this.isValid = false;
                        this.isValidErrorMsg = data.message
                    }
                } else {
                    this.isValid = false;
                    this.isValidErrorMsg = "Error occure please check the data"
                }
                this.loading = false;
            });
        } else if (this.isUpdate == true) {
            this.loading = true;
            return this.functionalServiceService.updatefunctions(formData).subscribe((data) => {
                if (data != undefined && data.success != undefined) {
                    if (data.success == true) {
                        alert("Updated");
                        this.isValid = true;
                        this.redirect();
                    } else {
                        this.isValid = false;
                        this.isValidErrorMsg = data.message
                    }
                } else {
                    this.isValid = false;
                    this.isValidErrorMsg = "Error occure please check the data"
                }
                this.loading = false;
            });
        }
    }

    validate() {
        if (this.functionalListResults.name != undefined && this.functionalListResults.name.toString().trim().length == 0) {
            this.functionalListResults.name = "";
        }
    }
    redirect() { this.router.navigate(['/functional-list']); }
}