import { Component } from '@angular/core';
import {CompetenciesAttributeService} from '../../../../service/competencies_attributes.service';
import {CompetenciesAttribute} from '../../../../model/competencies_attribute.model';
import {NgForm} from '@angular/forms';
import {ActivatedRoute,Router} from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';
import { routerTransition } from '../../../../router.animations';
@Component({

    selector : 'competenciesAttributeForm-Update',
    templateUrl: './competencies-attribute-update.component.html',
    providers:[CompetenciesAttributeService],
    animations: [routerTransition()]
  
})
export class CompetenciesAttributeFormAddUpdateComponent{

    isSave:boolean=true;
    isUpdate:boolean=true;
    isValid:boolean=true;
    isValidErrorMsg:string="";
    buttonValue:String="Save";
    public id:String =null;
    public competenciesAttributeResults:CompetenciesAttribute;
    competenciesAttribute = new CompetenciesAttribute();
    constructor(private competenciesAttributeService:CompetenciesAttributeService,
        private activatedRoute:ActivatedRoute,
        private router:Router) {
            this.competenciesAttributeResults = {
                "_id" : "",
                "attribute" : "",
                "attribute_description" : ""
            }
        }
   
    ngOnInit() {
        try {
            this.activatedRoute.params.subscribe( params => 
                {
                    this.id=params.id;
                } );
        } catch (error) {}
        if(this.id!=null){
            this.isUpdate=true;
            this.isSave=false;
    var res = this.competenciesAttributeService.getCompetenciesAttributeById(this.id);
    res.subscribe(results=>{
        console.log("results : "+results);
        if(results!=undefined&&results["data"]!=undefined){

            this.competenciesAttributeResults=results["data"] as CompetenciesAttribute;
            
        }
    });
    }else{
        this.isUpdate=false;
        this.isSave=true;
    }


    if((this.isSave == true) && (this.isUpdate == false)){
        this.buttonValue="Save";
     }
     if((this.isSave == false) && (this.isUpdate == true)){
        this.buttonValue="Update";
    }
    }
    onUpdate(formData){
        if(this.isSave == true){
            this.competenciesAttributeService.addCompetenciesAttribute(formData)
            .subscribe((res) => {
                console.log("res ---> "+res.message);
                if (res != undefined && res.success != undefined) {
                    if (res.success == true) {
                        alert("Added"); 
                        this.isValid=true;
                        this.redirect();
                    }else{
                        this.isValid=false;
                        this.isValidErrorMsg=res.message;
                    }
                }else{
                    this.isValid=false;
                    this.isValidErrorMsg="Error occure please check the data"
                }
            }
            );}
        else if(this.isUpdate == true){
            this.competenciesAttributeService.updateCompetenciesAttribute(formData)
            .subscribe((res) => {
                if (res != undefined && res.success != undefined) {
                    if (res.success == true) {
                        alert("Updated"); 
                        this.isValid=true;
                        this.redirect();
                    }else{
                        this.isValid=false;
                        this.isValidErrorMsg=res.message;
                    }
                }else{
                    this.isValid=false;
                    this.isValidErrorMsg="Error occure please check the data"
                }
            }
             );}
    }
    redirect() {this.router.navigate(['/competencies-attribute-form-list']); }
    validate(){    
        if(this.competenciesAttributeResults.attribute!=undefined && this.competenciesAttributeResults.attribute.toString().trim().length==0){
           this.competenciesAttributeResults.attribute="";
        }
      }
}