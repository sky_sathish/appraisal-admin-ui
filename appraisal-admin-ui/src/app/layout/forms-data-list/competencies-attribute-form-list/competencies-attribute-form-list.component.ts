import { Component } from '@angular/core';
import {CompetenciesAttributeService} from '../../../service/competencies_attributes.service';
import {CompetenciesAttribute} from '../../../model/competencies_attribute.model';
import { routerTransition } from '../../../router.animations';
@Component({

    selector : 'competenciesAttributeForm-List',
    templateUrl: './competencies-attribute-form-list.component.html',
    providers:[CompetenciesAttributeService],
    animations: [routerTransition()]
  
})
export class CompetenciesAttributeFormListComponent{

    public CompetenciesAttributeResults:CompetenciesAttribute[];
    constructor(private competenciesAttributeService:CompetenciesAttributeService) {}
    generalConfig = new CompetenciesAttribute();
  
    ngOnInit() {
    
    var res = this.competenciesAttributeService.getCompetenciesAttribute();
    res.subscribe(results=>{
        console.log("results : "+results);
        if(results!=undefined&&results["data"]!=undefined){

            this.CompetenciesAttributeResults=results["data"]["docs"] as CompetenciesAttribute[] ;
            
        }
    });
    }
    onDelete(data){
        if(confirm("Are you sure to delete?")) {
        return this.competenciesAttributeService.deleteCompetenciesAttribute(data._id).subscribe(()=>
        {
            var index = this.CompetenciesAttributeResults.indexOf(data, 0);
            if (index > -1)
            {
                this.CompetenciesAttributeResults.splice(index, 1);
            }
            // window.location.href = "/competencies-attribute-form-list";
         //   this.isDeleted=true;
        });
    }
    }
}