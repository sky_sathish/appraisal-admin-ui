import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { CompetenciesAttributeFormListRoutingModule } from './competencies-attribute-form-list-routing.module';
import { CompetenciesAttributeFormListComponent } from './competencies-attribute-form-list.component';
import { PageHeaderModule } from './../../../shared';
import { CompetenciesAttributeFormAddUpdateComponent } from './competencies-attribute-update/competencies-attribute-update.component';
@NgModule({
    imports: [CommonModule, CompetenciesAttributeFormListRoutingModule, PageHeaderModule,FormsModule],
    declarations: [CompetenciesAttributeFormListComponent,CompetenciesAttributeFormAddUpdateComponent]
})
export class CompetenciesAttributeFormListModule {}
