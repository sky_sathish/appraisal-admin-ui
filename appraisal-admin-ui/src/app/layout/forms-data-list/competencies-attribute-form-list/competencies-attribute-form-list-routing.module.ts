import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompetenciesAttributeFormListComponent } from './competencies-attribute-form-list.component';
import { CompetenciesAttributeFormAddUpdateComponent } from './competencies-attribute-update/competencies-attribute-update.component';
const routes: Routes = [
    {
        path: '', component: CompetenciesAttributeFormListComponent
    },
    {
        path: 'form', component: CompetenciesAttributeFormAddUpdateComponent
    },
    {
        path: 'form/:id', component: CompetenciesAttributeFormAddUpdateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CompetenciesAttributeFormListRoutingModule {
}