import { Component } from '@angular/core';
import { CompetenciesLevelService } from '../../../service/competencies_level.service';
import { CompetenciesLevel } from '../../../model/competencies_level.model';
import { Router } from '@angular/router';
import { routerTransition } from '../../../router.animations';
@Component({

    selector: 'competenciesLevel-List',
    templateUrl: './competenciesLevel-list.component.html',
    providers: [CompetenciesLevelService],
    animations: [routerTransition()]

})
export class CompetenciesLevelListComponent {

    competenciesLevelResults: CompetenciesLevel[];
    loading: boolean = false;
    constructor(private competenciesLevelService: CompetenciesLevelService,
        private router: Router) { }

    ngOnInit() {
        this.loading = true;
        this.competenciesLevelService.getCompetenciesLevel()
            .subscribe(results => {

                if (results != undefined && results["data"] != undefined) {

                    this.competenciesLevelResults = results["data"]["docs"] as CompetenciesLevel[];

                }
                this.loading = false;
            });

    }
    onDelete(data) {
        if (confirm("Are you sure to delete?")) {
            this.loading = true;
            return this.competenciesLevelService.deleteCompetenciesLevel(data._id).subscribe(() => {
                var index = this.competenciesLevelResults.indexOf(data, 0);
                if (index > -1) {
                    this.competenciesLevelResults.splice(index, 1);
                }
                this.loading = false;
                // window.location.href = "/competencies-level";
            });
        }
    }
    redirect() {

        this.router.navigate(['/competencies-level']);
    }
}