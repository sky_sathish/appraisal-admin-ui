import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { CompetenciesLevelListRoutingModule } from './competencies-level-list-routing.module';
import { CompetenciesLevelListComponent } from './competencies-level-list.component';
import { PageHeaderModule } from './../../../shared';

import { CompetenciesLevelFormUpdateComponent } from './competencies-level-form-update/competencies-level-form-update.component';

@NgModule({
    imports: [CommonModule, CompetenciesLevelListRoutingModule, PageHeaderModule,FormsModule],
    declarations: [CompetenciesLevelListComponent,CompetenciesLevelFormUpdateComponent]
})
export class CompetenciesLevelListModule {}
