import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompetenciesLevelListComponent } from './competencies-level-list.component';
import { CompetenciesLevelFormUpdateComponent } from './competencies-level-form-update/competencies-level-form-update.component';

const routes: Routes = [
    {
        path: '', component: CompetenciesLevelListComponent
    },
    {
        path: 'form', component: CompetenciesLevelFormUpdateComponent
    },
    {
        path: 'form/:id', component: CompetenciesLevelFormUpdateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CompetenciesLevelListRoutingModule {
}