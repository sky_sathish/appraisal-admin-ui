import { Component } from '@angular/core';
import {CompetenciesLevelService} from '../../../../service/competencies_level.service';
import {CompetenciesLevel} from '../../../../model/competencies_level.model';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';
import {NgForm} from '@angular/forms';
import {ActivatedRoute,Router} from '@angular/router';
import { routerTransition } from '../../../../router.animations';
@Component({

    selector : 'competencies-level-Form',
    templateUrl: './competencies-level-form-update.component.html',
    providers:[CompetenciesLevelService],
    animations: [routerTransition()]
  
})
export class CompetenciesLevelFormUpdateComponent{

    isSave:boolean=true;
    isUpdate:boolean=true;
    isValid:boolean=true;
    isValidErrorMsg:string="";
    buttonValue:String="Save";
    id:String =null;
    competenciesLevelResults:CompetenciesLevel;
    competenciesLevel=new CompetenciesLevel();
    loading:boolean =false;

    constructor(private activatedRoute:ActivatedRoute,
        private router:Router,
        private competenciesLevelService:CompetenciesLevelService) {

            this.competenciesLevelResults = {
                "_id" : "",
                "level_name" : "",
                "level_desc" : "",
                "sequence" : "",
                "weightage" : "",
                "rating_scale" : "",
                "scenario_1" : "",
                "scenario_2" : ""
         };
          
        }
// matric-look-for-last-year-list-add-update
        ngOnInit() {
            try {
                this.activatedRoute.params.subscribe( params => 
                    {
                        this.id=params.id;
                    } );
            } catch (error) {}
            if(this.id!=null){
            this.isUpdate=true;
            this.isSave=false;
            this.loading = true;
        var res = this.competenciesLevelService.getCompetenciesLevelById( this.id);
        res.subscribe(results=>{
            console.log("results : "+results);
            if(results!=undefined&&results["data"]!=undefined){
    
               this.competenciesLevelResults=results["data"] as CompetenciesLevel ;
                
            }
            this.loading = false;
        }); 
            }else{
                this.isUpdate=false;
                this.isSave=true;
            }


            if((this.isSave == true) && (this.isUpdate == false)){
                this.buttonValue="Save";
             }
             if((this.isSave == false) && (this.isUpdate == true)){
                this.buttonValue="Update";
            }
        }

        
    onAddData(formData){

        if(this.isSave == true){
            this.loading = true;
        return this.competenciesLevelService.addCompetenciesLevel(formData)
        .subscribe((res) => {
            if (res != undefined && res.success != undefined) {
                if (res.success == true) {
                    alert("Added"); 
                    this.isValid=true;
                    this.redirect();
                }else{
                    this.isValid=false;
                    this.isValidErrorMsg=res.message;
                }
            }else{
                this.isValid=false;
                this.isValidErrorMsg="Error occure please check the data"
            }
            this.loading = false;
        }
        );}
        else if(this.isUpdate == true){
            this.loading = true;
          return this.competenciesLevelService.updateCompetenciesLevel(formData)
          .subscribe((res) => {
            if (res != undefined && res.success != undefined) {
                if (res.success == true) {
                    alert("Updated"); 
                    this.isValid=true;
                    this.redirect();
                }else{
                    this.isValid=false;
                    this.isValidErrorMsg=res.message;
                }
            }else{
                this.isValid=false;
                this.isValidErrorMsg="Error occure please check the data"
            }
            this.loading = false;
        }
         );}
}
    redirect() {this.router.navigate(['/competencies-level']); }
    validate(){    
        if(this.competenciesLevelResults.level_name!=undefined && this.competenciesLevelResults.level_name.toString().trim().length==0){
           this.competenciesLevelResults.level_name="";
        }
        if(this.competenciesLevelResults.sequence!=undefined && this.competenciesLevelResults.sequence.toString().trim().length==0){
            this.competenciesLevelResults.sequence="";
         }
         if(this.competenciesLevelResults.weightage!=undefined && this.competenciesLevelResults.weightage.toString().trim().length==0){
            this.competenciesLevelResults.weightage="";
         }
         if(this.competenciesLevelResults.rating_scale!=undefined && this.competenciesLevelResults.rating_scale.toString().trim().length==0){
            this.competenciesLevelResults.rating_scale="";
         }
         if(this.competenciesLevelResults.scenario_1!=undefined && this.competenciesLevelResults.scenario_1.toString().trim().length==0){
            this.competenciesLevelResults.scenario_1="";
         }
         if(this.competenciesLevelResults.scenario_2!=undefined && this.competenciesLevelResults.scenario_2.toString().trim().length==0){
            this.competenciesLevelResults.scenario_2="";
         }
      }
}