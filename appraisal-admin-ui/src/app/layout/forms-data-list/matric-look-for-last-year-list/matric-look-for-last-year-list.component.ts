import { Component,OnInit } from '@angular/core';
import {MatrixLookForLastYearService} from '../../../service/matric_look_for_last_year.service';
import {MatrixLookForLastYear} from '../../../model/matric_look_for_last_year.model';
import { routerTransition } from '../../../router.animations';
import { LocalDataSource } from 'ng2-smart-table';
@Component({

    selector : 'matricLockForm-List',
    templateUrl: './matric-look-for-last-year-list.component.html',
    providers:[MatrixLookForLastYearService],
    animations: [routerTransition()]
  
})
export class MatricLookForLastYearListComponent{

   
    public matricsResult:MatrixLookForLastYear[];
    isDeleted : boolean =false;
    switchOnOff = false;
    perPageRecord = 5;
    source: LocalDataSource;

     // smart table (documentaion link:https://akveo.github.io/ng2-smart-table/#/documentation)
     settings = {
        columns: {
            seq: {
                title: 'No.',
                editable: false,
                width: '10%'
            },
            name: {
                title: 'Matrix Name',
                editable: false,
                width: '90%'
            }
        },
        mode: 'external',//'inline'(can edit user-side) | 'external'(can not be edit user-side)
        hideHeader: false,
        hideSubHeader: false,// eg.search field
        noDataMessage: "Data not found!!!",
        actions: {
            add: false,
            edit: false,
            delete: false,
            custom: [
                { name: 'editCall', title: '<i class="fa fa-edit"></i>&nbsp;' },
                { name: 'deleteCall', title: '<i class="fa fa-trash"></i>&nbsp;' }
            ]

        },
        pager: {
            display: true,
            perPage: this.perPageRecord
        }
    };

    constructor(private matrixLookForLastYearService:MatrixLookForLastYearService) {
        this.source = new LocalDataSource();
        var res = this.matrixLookForLastYearService.getMatrixLookForLastYear();
    res.subscribe(results=>{
        if(results!=undefined&&results["data"]!=undefined){
            this.matricsResult=results["data"]["docs"] as MatrixLookForLastYear[];
            var loadData:any = [];
            var count=0;
            for(var i=0;i<this.matricsResult.length;i++){
                if(this.matricsResult[i] != undefined && this.matricsResult[i] != null){
                    count = count +1;
                    loadData.push({
                        "seq" : count,
                        "name" : this.matricsResult[i]["name"],
                        "id" : this.matricsResult[i]["_id"]
                    })
                }
            }
            this.source.load(loadData);
        }
    });
    }
    

    ngOnInit() {
    
    // var res = this.matrixLookForLastYearService.getMatrixLookForLastYear();
    // res.subscribe(results=>{
    //     if(results!=undefined&&results["data"]!=undefined){

    //         this.matricsResult=results["data"]["docs"] as MatrixLookForLastYear[] ;
        
    //     }
    // });
    }
    

    onCustomAction(event) {

        if (event["action"] != undefined) {
            var id = null;
            id = event["data"]["id"];

            if (event["action"] == "editCall" && id !=null && id !=undefined) {
                window.location.href = "/matric-look-for-last-year-list/form/" + id;
            }
            if (event["action"] == "deleteCall" && id !=null && id !=undefined) {
                if (confirm("Are you sure to delete?")) {
                    return this.matrixLookForLastYearService.deleteMatrixLookForLastYear(id).then(result => {
                        this.source.remove(event["data"]).then(ele=>{
                         });
                    });
                }

            }
        }
    }
    switchTable(){
        if(this.switchOnOff == true){
            this.switchOnOff = false;
        }else{
        this.switchOnOff = true;
        }
    }
    onDelete(data){
    //     if(confirm("Are you sure to delete?")) {
    // return this.matrixLookForLastYearService.deleteMatrixLookForLastYear(data._id).subscribe(results=>
    // {
    //     var index = this.matricsResult.indexOf(data, 0);
    //     if (index > -1)
    //     {
    //         this.matricsResult.splice(index, 1);
    //     }
    //     // window.location.href = "/matric-look-for-last-year-list";
    // });
// }
    }
}