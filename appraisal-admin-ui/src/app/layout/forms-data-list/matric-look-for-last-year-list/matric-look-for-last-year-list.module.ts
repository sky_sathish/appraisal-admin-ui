import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { MatricLookForLastYearListRoutingModule } from './matric-look-for-last-year-list-routing.module';
import { MatricLookForLastYearListComponent } from './matric-look-for-last-year-list.component';
import { PageHeaderModule } from './../../../shared';
import { MatricLookForLastYearFormAddUpdateComponent } from './matric-look-for-last-year-list-add-update/matric-look-for-last-year-list-add-update.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
@NgModule({
    imports: [
        CommonModule, 
        MatricLookForLastYearListRoutingModule,
         PageHeaderModule,
         FormsModule,
         Ng2SmartTableModule
        ],
    declarations: [MatricLookForLastYearListComponent,MatricLookForLastYearFormAddUpdateComponent]
})
export class MatricLookForLastYearListModule {}
