import { Component, OnInit } from '@angular/core';
import { MatrixLookForLastYearService } from '../../../../service/matric_look_for_last_year.service';
import { MatrixLookForLastYear } from '../../../../model/matric_look_for_last_year.model';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { routerTransition } from '../../../../router.animations';
@Component({

    selector: 'matricLockForm-Update',
    templateUrl: './matric-look-for-last-year-list-add-update.component.html',
    providers: [MatrixLookForLastYearService],
    animations: [routerTransition()]

})
export class MatricLookForLastYearFormAddUpdateComponent {

    isSave: boolean = true;
    isUpdate: boolean = true;
    isValid:boolean=true;
    isValidErrorMsg:string="";
    buttonValue: String = "Save";
    public id: String = null;
    public matricsResult: MatrixLookForLastYear;
    constructor(private matrixLookForLastYearService: MatrixLookForLastYearService,
        private activatedRoute: ActivatedRoute,
        private router: Router) {
        this.matricsResult = {
            "_id": "",
            "name": ""
        }
    }
    matrixLookForLastYear = new MatrixLookForLastYear();

    ngOnInit() {
        try {
            this.activatedRoute.params.subscribe(params => {
                this.id = params.id;
            });
        } catch (error) { }

        if (this.id != null) {
            this.isUpdate = true;
            this.isSave = false;
            var res = this.matrixLookForLastYearService.getMatrixLookForLastYearById(this.id);
            res.subscribe(results => {

                if (results != undefined && results["data"] != undefined) {

                    this.matricsResult = results["data"] as MatrixLookForLastYear;
                    console.log("initial log update->name: " + this.matricsResult.name + " --- id :" + this.matricsResult._id);
                }
            });
        } else {
            this.isUpdate = false;
            this.isSave = true;
        }

        if ((this.isSave == true) && (this.isUpdate == false)) {
            this.buttonValue = "Save";
        }
        if ((this.isSave == false) && (this.isUpdate == true)) {
            this.buttonValue = "Update";
        }

    }
    onUpdate(formData) {
        if (this.isSave == true) {
            this.matrixLookForLastYearService.addMatrixLookForLastYear(formData)
                .subscribe((res) => {
                    if (res != undefined && res.success != undefined) {
                        if (res.success == true) {
                            alert("Added"); 
                            this.isValid=true;
                            this.redirect();
                        }else{
                            this.isValid=false;
                            this.isValidErrorMsg=res.message;
                        }
                    }else{
                        this.isValid=false;
                        this.isValidErrorMsg="Error occure please check the data"
                    }
                }
                );}
        else if (this.isUpdate == true) {
            this.matrixLookForLastYearService.updateMatrixLookForLastYear(formData)
            .subscribe((res) => {
                if (res != undefined && res.success != undefined) {
                    if (res.success == true) {
                        alert("Updated"); 
                        this.isValid=true;
                        this.redirect();
                    }else{
                        this.isValid=false;
                        this.isValidErrorMsg=res.message;
                    }
                }else{
                    this.isValid=false;
                    this.isValidErrorMsg="Error occure please check the data"
                }
            }
             );}
    }
    redirect() { this.router.navigate(['/matric-look-for-last-year-list']); }

    validate() {
        if (this.matricsResult.name != undefined && this.matricsResult.name.toString().trim().length == 0) {
            this.matricsResult.name = "";
        }
    }

}