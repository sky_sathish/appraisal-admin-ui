import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MatricLookForLastYearListComponent } from './matric-look-for-last-year-list.component';
import { MatricLookForLastYearFormAddUpdateComponent } from './matric-look-for-last-year-list-add-update/matric-look-for-last-year-list-add-update.component';
const routes: Routes = [
    {
        path: '', component: MatricLookForLastYearListComponent
    },
    {
        path: 'form', component: MatricLookForLastYearFormAddUpdateComponent
    },
    {
        path: 'form/:id', component: MatricLookForLastYearFormAddUpdateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MatricLookForLastYearListRoutingModule {
}
