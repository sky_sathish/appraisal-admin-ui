import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompetenciesFunctionalAttributesListComponent } from './competencies-functional-attribute-list.component';

const routes: Routes = [
    {
        path: '', component: CompetenciesFunctionalAttributesListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CompetenciesFunctionalAttributesListRoutingModule {
}