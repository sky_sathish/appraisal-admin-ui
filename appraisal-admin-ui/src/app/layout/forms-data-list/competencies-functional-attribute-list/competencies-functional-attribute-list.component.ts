import { Component } from '@angular/core';
import {CompetenciesFunctionalAttributesService} from '../../../service/competencies_functional_attributes.service';
import {CompetenciesFunctionalAttributes} from '../../../model/competencies_functional_attributes.model';
import { CompetenciesLevelService } from '../../../service/competencies_level.service';
import { RoleService } from '../../../service/role.service';
import { HomeService } from '../../../service/home.service';
import {CompetenciesAttributeService} from '../../../service/competencies_attributes.service';
import { routerTransition } from '../../../router.animations';
@Component({

    selector : 'CompetenciesFunctionalAttributes-List',
    templateUrl: './competencies-functional-attribute-list.component.html',
    providers:[CompetenciesFunctionalAttributesService,
        HomeService,
        CompetenciesAttributeService,CompetenciesLevelService,RoleService],
        animations: [routerTransition()]
  
})
export class CompetenciesFunctionalAttributesListComponent{
    constructor(private competenciesFunctionalAttributesService:CompetenciesFunctionalAttributesService) {}
    public competenciesFunctionalAttributesResults:CompetenciesFunctionalAttributes[];
    competenciesFunctionalAttributes = new CompetenciesFunctionalAttributes();
  
    ngOnInit() {
    // competenciesFunctionalAttributesService
    var res = this.competenciesFunctionalAttributesService.getCompetenciesFunctionalAttributes();
    res.subscribe(results=>{
       
        if(results!=undefined&&results["data"]!=undefined){
            this.competenciesFunctionalAttributesResults=results["data"] as CompetenciesFunctionalAttributes[];
        }
    });
    }

    onDelete(data){
        if(confirm("Are you sure to delete?")) {
        return this.competenciesFunctionalAttributesService.deleteCompetenciesFunctionalAttributes(data._id).subscribe(()=>
        {
            var index = this.competenciesFunctionalAttributesResults.indexOf(data, 0);
            if (index > -1)
            {
                this.competenciesFunctionalAttributesResults.splice(index, 1);
            }
            // window.location.href = "/competencies-functional-attributes-list";
        });
    }
    }
}