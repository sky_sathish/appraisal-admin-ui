import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { CompetenciesFunctionalAttributesListRoutingModule } from './competencies-functional-attribute-list-routing.module';
import { CompetenciesFunctionalAttributesListComponent } from './competencies-functional-attribute-list.component';
import { PageHeaderModule } from './../../../shared';

@NgModule({
    imports: [CommonModule, CompetenciesFunctionalAttributesListRoutingModule, PageHeaderModule,FormsModule],
    declarations: [CompetenciesFunctionalAttributesListComponent]
})
export class CompetenciesFunctionalAttributesListModule {}
