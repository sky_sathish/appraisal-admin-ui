import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { GoalAttributeFormListRoutingModule } from './goal-attribute-form-list-routing.module';
import { GoalAttributeFormListComponent } from './goal-attribute-form-list.component';
import { PageHeaderModule } from './../../../shared';
import { Ng2SmartTableModule } from 'ng2-smart-table';

@NgModule({
    imports: [CommonModule, GoalAttributeFormListRoutingModule, PageHeaderModule,FormsModule,Ng2SmartTableModule],
    declarations: [GoalAttributeFormListComponent]
})
export class GoalAttributeFormListModule {}
