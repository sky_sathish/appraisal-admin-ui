import { Component } from '@angular/core';
import {GoalAttributeService} from '../../../service/goal_attribute.service';
import {GoalAttribute} from '../../../model/goal_attribute.model';
import {SubcategoryService} from '../../../service/goal_subcategory.service';
import {Subcategory} from '../../../model/goal_subcategory.model';
import { routerTransition } from '../../../router.animations';
import { LocalDataSource } from 'ng2-smart-table';
@Component({

    selector : 'goalAttribute-List',
    templateUrl: './goal-attribute-form-list.component.html',
    providers:[GoalAttributeService,SubcategoryService],
    animations: [routerTransition()]
  
})
export class GoalAttributeFormListComponent{

    public goalAttributeResults:GoalAttribute[];
    public subcategoryResults:Subcategory[];
    goalAttribute = new GoalAttribute();
    subcategory = new Subcategory();
    switchOnOff = false;
    perPageRecord = 5;
    source: LocalDataSource;

     // smart table (documentaion link:https://akveo.github.io/ng2-smart-table/#/documentation)
     settings = {
        columns: {
            seq: {
                title: 'No.',
                editable: false,
                width: '10%'
            },
            attribute: {
                title: 'Attribute Name',
                editable: false
            },
            attribute_category: {
                title: 'Category',
                editable: false
            },
            attribute_sub_category: {
                title: 'Sub Category',
                editable: false
            }
        },
        mode: 'external',//'inline'(can edit user-side) | 'external'(can not be edit user-side)
        hideHeader: false,
        hideSubHeader: false,// eg.search field
        noDataMessage: "Data not found!!!",
        actions: {
            add: false,
            edit: false,
            delete: false,
            custom: [
                { name: 'editCall', title: '<i class="fa fa-edit"></i>&nbsp;' },
                { name: 'deleteCall', title: '<i class="fa fa-trash"></i>&nbsp;' }
            ]

        },
        pager: {
            display: true,
            perPage: this.perPageRecord
        }
    };
    constructor(private goalAttributeService:GoalAttributeService,
                private subcategoryService:SubcategoryService) {
                    this.source = new LocalDataSource();

                    var goal_res = this.goalAttributeService.getGoalAttribute();
    goal_res.subscribe(results=>{
        if(results!=undefined&&results["data"]!=undefined){
            this.goalAttributeResults=results["data"] as GoalAttribute[] ;
            var loadData:any = [];
            var count=0;
            for(var i=0;i<this.goalAttributeResults.length;i++){
                if(this.goalAttributeResults[i] != undefined && this.goalAttributeResults[i] != null){
                    count = count +1;
                    loadData.push({
                        "seq" : count,
                        "attribute" : this.goalAttributeResults[i]["attribute"],
                        "attribute_category" : this.goalAttributeResults[i]["attribute_category"]["name"],
                        "attribute_sub_category" : this.goalAttributeResults[i]["attribute_sub_category"]["name"],
                        "id" : this.goalAttributeResults[i]["_id"]
                    })
                }
            }
            this.source.load(loadData);
            
        }
    });
                }
   
    ngOnInit() {
    
    // var goal_res = this.goalAttributeService.getGoalAttribute();
    // goal_res.subscribe(results=>{
       
    //     if(results!=undefined&&results["data"]!=undefined){

    //         this.goalAttributeResults=results["data"] as GoalAttribute[] ;
           
    //     }
    // });

    // var goal_res = this.subcategoryService.getSubcategory();
    // goal_res.subscribe(results=>{
       
    //     if(results!=undefined&&results["data"]!=undefined){

    //         this.subcategoryResults=results["data"]["docs"] as Subcategory[] ;
            
    //     }
    // });
    }
    onCustomAction(event) {

        if (event["action"] != undefined) {
            var id = null;
            id = event["data"]["id"];

            if (event["action"] == "editCall" && id !=null && id !=undefined) {
                window.location.href = "/goal-attribute-form/" + id;
            }
            if (event["action"] == "deleteCall" && id !=null && id !=undefined) {
                if (confirm("Are you sure to delete?")) {
                    return this.goalAttributeService.deleteGoalAttribute(id).then(result => {
                        this.source.remove(event["data"]).then(ele=>{
                         });
                    });
                }

            }
        }
    }
    switchTable(){
        if(this.switchOnOff == true){
            this.switchOnOff = false;
        }else{
        this.switchOnOff = true;
        }
    }
    onDelete(data){
        if(confirm("Are you sure to delete?")) {
            return this.goalAttributeService.deleteGoalAttribute(data._id).subscribe(()=>
        {
            var index = this.goalAttributeResults.indexOf(data, 0);
                if (index > -1)
                {
                    this.goalAttributeResults.splice(index, 1);
                }
            // window.location.href = "/goal-attribute-list";
        });
    }
    }
}