import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoalAttributeFormListComponent } from './goal-attribute-form-list.component';

const routes: Routes = [
    {
        path: '', component: GoalAttributeFormListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GoalAttributeFormListRoutingModule {
}