import { Component } from '@angular/core';
import { Role } from '../../../model/role.model';
import { RoleService } from '../../../service/role.service';
import { routerTransition } from '../../../router.animations';
import { LocalDataSource } from 'ng2-smart-table';
@Component({

    selector: 'role-List',
    templateUrl: './role-list.component.html',
    providers: [RoleService],
    animations: [routerTransition()]

})
export class RoleListComponent {

    public roleResults: Role[];
    switchOnOff = false;
    perPageRecord = 5;
    source: LocalDataSource;
    loading: boolean = false;

    constructor(private roleService: RoleService) { }

    // smart table (documentaion link:https://akveo.github.io/ng2-smart-table/#/documentation)
    settings = {
        columns: {
            seq: {
                title: 'No.',
                editable: false,
                width: '10%'
            },
            name: {
                title: 'Role Name',
                editable: false,
                width: '90%'
            }
        },
        mode: 'external',//'inline'(can edit user-side) | 'external'(can not be edit user-side)
        hideHeader: false,
        hideSubHeader: false,// eg.search field
        noDataMessage: "Data not found!!!",
        actions: {
            add: false,
            edit: false,
            delete: false,
            custom: [
                { name: 'editCall', title: '<i class="fa fa-edit"></i>&nbsp;' },
                { name: 'deleteCall', title: '<i class="fa fa-trash"></i>&nbsp;' }
            ],

        },
        pager: {
            display: true,
            perPage: this.perPageRecord
        }
    };

    ngOnInit() {
        this.loading = true;
        this.roleService.getRole()
            .then(results => {
                if (results != undefined && results["data"] != undefined) {
                    this.roleResults = results["data"] as Role[];
                    this.source = new LocalDataSource();
                    this.onExtract(this.roleResults);
                }
                this.loading = false;
            });
    }


    onCustomAction(event) {

        if (event["action"] != undefined) {
            var id = null;
            id = event["data"]["id"];

            if (event["action"] == "editCall" && id != null && id != undefined) {
                window.location.href = "/role-list/form/" + id;
            }
            if (event["action"] == "deleteCall" && id != null && id != undefined) {
                if (confirm("Are you sure to delete?")) {
                    this.loading = true;
                    return this.roleService.deleteRole(id).then(result => {
                        this.source.remove(event["data"]).then(ele => {
                        });
                        this.loading = false;
                    });
                }

            }
        }
    }
    onExtract(result) {
        if (result != undefined) {
            var data = [];
            var originalData = [];
            data = result;
            var count = 0;
            for (var i = 0; i < data.length; i++) {
                count = count + 1;
                originalData.push(
                    {
                        "seq": count,
                        "name": data[i].name,
                        "id": data[i]._id
                    }
                );
            }
            this.source.load(originalData);
        }
    }
    autoAddRolesList() {
        this.loading = true;
        return this.roleService.autoAddRoles({}).subscribe((result) => {
            alert("Roles successfully added.")
            this.loading = false;
        });
    }
    switchTable() {
        if (this.switchOnOff == true) {
            this.switchOnOff = false;
        } else {
            this.switchOnOff = true;
        }
    }
    onDelete(data) {
        if (confirm("Are you sure to delete?")) {
            this.loading = true;
            return this.roleService.deleteRole(data._id).then(() => {

                var index = this.roleResults.indexOf(data, 0);
                if (index > -1) {
                    this.roleResults.splice(index, 1);
                }
                alert("deleted")
                this.loading = false;
            });
        }

    }
}