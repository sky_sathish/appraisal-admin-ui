import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleListComponent } from './role-list.component';

import { RoleUpdateComponent } from './role-list-form-update/role-update.component';

const routes: Routes = [
    {
        path: '', component: RoleListComponent
    },
    {
        path: 'form', component: RoleUpdateComponent
    },
    {
        path: 'form/:id', component: RoleUpdateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RoleListRoutingModule {
}
