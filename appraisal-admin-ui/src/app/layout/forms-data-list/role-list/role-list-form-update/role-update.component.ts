import { Component } from '@angular/core';
import { Role } from '../../../../model/role.model';
import { RoleService } from '../../../../service/role.service';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { routerTransition } from '../../../../router.animations';
@Component({

    selector: 'role-List',
    templateUrl: './role-update.component.html',
    providers: [RoleService],
    animations: [routerTransition()]

})
export class RoleUpdateComponent {

    isSave: boolean = true;
    isUpdate: boolean = true;
    isValid: boolean = true;
    isValidErrorMsg: string = "";
    isDuplicate: boolean = false;
    buttonValue: String = "Save";
    id: String = null;
    roleResults: Role;
    role = new Role();
    loading: boolean = false;

    constructor(private activatedRoute: ActivatedRoute,
        private router: Router,
        private roleService: RoleService) {
        this.roleResults = {
            "_id": "",
            "name": ""
        }
    }

    ngOnInit() {
        try {
            this.activatedRoute.params.subscribe(params => {
                this.id = params.id;
            });
        } catch (error) { }
        if (this.id != null) {
            this.isUpdate = true;
            this.isSave = false;
            this.loading = true;
            var res = this.roleService.getRoleById(this.id);
            res.subscribe(results => {
                console.log("results : " + results);
                if (results != undefined && results["data"] != undefined) {

                    this.roleResults = results["data"] as Role;

                }
                this.loading = false;
            });
        } else {
            this.isUpdate = false;
            this.isSave = true;
        }
        if ((this.isSave == true) && (this.isUpdate == false)) {
            this.buttonValue = "Save";
        }
        if ((this.isSave == false) && (this.isUpdate == true)) {
            this.buttonValue = "Update";
        }
    }

    onUpdate(formData) {
        if (this.isSave == true) {
            this.loading = true;
            return this.roleService.addRole(formData).subscribe((data) => {
                if (data != undefined && data.success != undefined) {
                    if (data.success == true) {
                        alert("Added");
                        this.isValid = true;
                        this.redirect();
                    } else {
                        this.isValid = false;
                        this.isValidErrorMsg = data.message
                    }
                } else {
                    this.isValid = false;
                    this.isValidErrorMsg = "Error occure please check the data"
                }
                this.loading = false;
            });
        } else if (this.isUpdate == true) {
            this.loading = true;
            return this.roleService.updateRole(formData).subscribe((data) => {

                if (data != undefined && data.success != undefined) {
                    if (data.success == true) {
                        alert("Updated");
                        this.isValid = true;
                        this.redirect();
                    } else {
                        this.isValid = false;
                        this.isValidErrorMsg = data.message
                    }
                } else {
                    this.isValid = false;
                    this.isValidErrorMsg = "Error occure please check the data"
                }
                this.loading = false;
            });
        }
    }

    validate() {
        if (this.roleResults.name != undefined && this.roleResults.name.toString().trim().length == 0) {
            this.roleResults.name = "";
        }
    }
    redirect() { this.router.navigate(['/role-list']); }
}