import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { RoleListRoutingModule } from './role-list-routing.module';
import { RoleListComponent } from './role-list.component';
import { PageHeaderModule } from './../../../shared';
import { RoleUpdateComponent } from './role-list-form-update/role-update.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
@NgModule({
    imports: [
        CommonModule, 
        RoleListRoutingModule, 
        PageHeaderModule,
        FormsModule,
        Ng2SmartTableModule
    ],
    declarations: [RoleListComponent,RoleUpdateComponent]
})
export class RoleListModule {}
