import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralConfigFormListComponent } from './general-config-form-list.component';
import { GeneralConfigFormAddUpdateComponent } from './general-config-form-add-update/general-config-form-update.component';

const routes: Routes = [
    {
        path: '', component: GeneralConfigFormListComponent
    },
    {
        path: 'form', component: GeneralConfigFormAddUpdateComponent
    },
    {
        path: 'form/:id', component: GeneralConfigFormAddUpdateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GeneralConfigFormListRoutingModule {
}