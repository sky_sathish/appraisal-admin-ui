import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { GeneralConfigFormListRoutingModule } from './general-config-form-list-routing.module';
import { GeneralConfigFormListComponent } from './general-config-form-list.component';
import { PageHeaderModule } from './../../../shared';
import { GeneralConfigFormAddUpdateComponent } from './general-config-form-add-update/general-config-form-update.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
    imports: [CommonModule, GeneralConfigFormListRoutingModule, PageHeaderModule,NgbModule.forRoot(),FormsModule],
    declarations: [GeneralConfigFormListComponent,GeneralConfigFormAddUpdateComponent]
})
export class GeneralConfigFormListModule {}
