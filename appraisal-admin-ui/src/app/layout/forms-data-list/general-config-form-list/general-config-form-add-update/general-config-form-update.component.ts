import { Component } from '@angular/core';
import {GeneralConfigService} from '../../../../service/general_config.service';
import {GeneralConfig} from '../../../../model/general_config.model';
import {NgForm} from '@angular/forms';
import {ActivatedRoute,Router} from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';
import { routerTransition } from '../../../../router.animations';
@Component({

    selector : 'generalConfigForm-Update',
    templateUrl: './general-config-form-update.component.html',
    providers:[GeneralConfigService],
    animations: [routerTransition()]
  
})
export class GeneralConfigFormAddUpdateComponent{

    isSave:boolean=true;
    isUpdate:boolean=true;
    isValid:boolean=true;
    isValidErrorMsg:string="";
    buttonValue:String="Save";
    public isCollapsed = true;
    public id:String =null;
    public generalConfigResults:GeneralConfig;
    // generalConfig = new GeneralConfig();
    constructor(private generalConfigService:GeneralConfigService,
        private activatedRoute:ActivatedRoute,
        private router:Router) {

            this.generalConfigResults = {
                "_id":"",
                "key" : "",
                "key_desc" : "",
                "tag" : "",
                "sequence" : 0,
                "value" : "",
                "description" : ""
            }
        }
   
  
    ngOnInit() {
        try {
            this.activatedRoute.params.subscribe( params => 
                {
                    this.id=params.id;
                } );
        } catch (error) {}
      
        if(this.id!=null){
            this.isUpdate=true;
            this.isSave=false;
    var res = this.generalConfigService.getGeneralConfigById(this.id);
    res.subscribe(results=>{
        console.log("getGeneralConfigById results : "+results);
        if(results!=undefined&&results["data"]!=undefined){

            this.generalConfigResults=results["data"] as GeneralConfig ;
            console.log(this.generalConfigResults.key);
        }
    });
    }else{
        this.isUpdate=false;
        this.isSave=true;
    }
    if((this.isSave == true) && (this.isUpdate == false)){
        this.buttonValue="Save";
     }
     if((this.isSave == false) && (this.isUpdate == true)){
        this.buttonValue="Update";
    }
    }
    onUpdate(formData){
        if(this.isSave == true){
            this.generalConfigService.addGeneralConfig(formData)
            .subscribe((res) => {
                if (res != undefined && res.success != undefined) {
                    if (res.success == true) {
                        alert("Added"); 
                        this.isValid=true;
                        this.redirect();
                    }else{
                        this.isValid=false;
                        this.isValidErrorMsg=res.message;
                    }
                }else{
                    this.isValid=false;
                    this.isValidErrorMsg="Error occure please check the data"
                }
            }
            );}
        else if(this.isUpdate == true){
        return this.generalConfigService.updateGeneralConfig(formData)
        .subscribe((res) => {
            if (res != undefined && res.success != undefined) {
                if (res.success == true) {
                    alert("Updated"); 
                    this.isValid=true;
                    this.redirect();
                }else{
                    this.isValid=false;
                    this.isValidErrorMsg=res.message;
                }
            }else{
                this.isValid=false;
                this.isValidErrorMsg="Error occure please check the data"
            }
        }
         );}
}
redirect() {this.router.navigate(['/general-config-list']); }
validate(){    
    if(this.generalConfigResults.key!=undefined && this.generalConfigResults.key.toString().trim().length==0){
       this.generalConfigResults.key="";
    }
    if(this.generalConfigResults.value!=undefined && this.generalConfigResults.value.toString().trim().length==0){
        this.generalConfigResults.value="";
     }
     if(this.generalConfigResults.tag!=undefined && this.generalConfigResults.tag.toString().trim().length==0){
        this.generalConfigResults.tag="";
     }
     
  }
}