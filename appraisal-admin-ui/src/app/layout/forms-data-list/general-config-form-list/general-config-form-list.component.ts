import { Component } from '@angular/core';
import {GeneralConfigService} from '../../../service/general_config.service';
import {GeneralConfig} from '../../../model/general_config.model';
import { routerTransition } from '../../../router.animations';
@Component({

    selector : 'generalConfigForm-List',
    templateUrl: './general-config-form-list.component.html',
    providers:[GeneralConfigService],
    animations: [routerTransition()]
  
})
export class GeneralConfigFormListComponent{

    public generalConfigResults:GeneralConfig[];
    constructor(private generalConfigService:GeneralConfigService) {}
    generalConfig = new GeneralConfig();
    ngOnInit() {
    
    var res = this.generalConfigService.getGeneralConfig();
    res.subscribe(results=>{
        console.log("results : "+results);
        if(results!=undefined&&results["data"]!=undefined){

            this.generalConfigResults=results["data"]["docs"] as GeneralConfig[] ;
            
        }
    });
    }
    onDelete(data){
        if(confirm("Are you sure to delete?")) {
        return this.generalConfigService.deleteGeneralConfig(data._id).subscribe(()=>
        {
            var index = this.generalConfigResults.indexOf(data, 0);
            if (index > -1)
            {
                this.generalConfigResults.splice(index, 1);
            }
            // window.location.href = "/general-config-list";
        });
    }
    }
}