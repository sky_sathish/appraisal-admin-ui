import { Component } from '@angular/core';
import { CategoryService } from '../../../service/goal_category.service';
import { Category } from '../../../model/goal_category.model';
import { routerTransition } from '../../../router.animations';
import { LocalDataSource } from 'ng2-smart-table';
@Component({

    selector: 'goalcategory-List',
    templateUrl: './goal-category-list.component.html',
    providers: [CategoryService],
    animations: [routerTransition()]


})
export class GoalCategoryListComponent {

    CategoryResults: Category[];
    switchOnOff = false;
    perPageRecord = 5;
    source: LocalDataSource;
    loading: boolean = false;
    // smart table (documentaion link:https://akveo.github.io/ng2-smart-table/#/documentation)
    settings = {
        columns: {
            seq: {
                title: 'No.',
                editable: false,
                width: '10%'
            },
            name: {
                title: 'Goal Category Name',
                editable: false,
                width: '90%'
            }
        },
        mode: 'external',//'inline'(can edit user-side) | 'external'(can not be edit user-side)
        hideHeader: false,
        hideSubHeader: false,// eg.search field
        noDataMessage: "Data not found!!!",
        actions: {
            add: false,
            edit: false,
            delete: false,
            custom: [
                { name: 'editCall', title: '<i class="fa fa-edit"></i>&nbsp;' },
                { name: 'deleteCall', title: '<i class="fa fa-trash"></i>&nbsp;' }
            ]

        },
        pager: {
            display: true,
            perPage: this.perPageRecord
        }
    };
    constructor(private categoryService: CategoryService) {

    }

    ngOnInit() {

        this.source = new LocalDataSource();
        var res = this.categoryService.getCategory();
        this.loading = true;
        res.subscribe(results => {
            if (results != undefined && results["data"] != undefined) {
                this.CategoryResults = results["data"]["docs"] as Category[];
                var loadData: any = [];
                var count = 0;
                for (var i = 0; i < this.CategoryResults.length; i++) {
                    if (this.CategoryResults[i] != undefined && this.CategoryResults[i] != null) {
                        count = count + 1;
                        loadData.push({
                            "seq": count,
                            "name": this.CategoryResults[i]["name"],
                            "id": this.CategoryResults[i]["_id"]
                        })
                    }
                }
                this.source.load(loadData);

            }
            this.loading = false;
        });
    }

    onCustomAction(event) {

        if (event["action"] != undefined) {
            var id = null;
            id = event["data"]["id"];

            if (event["action"] == "editCall" && id != null && id != undefined) {
                window.location.href = "/goalsubcategory-list/form/" + id;
            }
            if (event["action"] == "deleteCall" && id != null && id != undefined) {
                if (confirm("Are you sure to delete?")) {
                    return this.categoryService.deleteCategory(id).then(result => {
                        this.source.remove(event["data"]).then(ele => {
                        });
                    });
                }

            }
        }
    }
    switchTable() {
        if (this.switchOnOff == true) {
            this.switchOnOff = false;
        } else {
            this.switchOnOff = true;
        }
    }
    onDelete(data) {

        if (confirm("Are you sure to delete?")) {
            this.loading = true;
            return this.categoryService.deleteCategory(data._id).then(() => {
                var index = this.CategoryResults.indexOf(data, 0);
                if (index > -1) {
                    this.CategoryResults.splice(index, 1);
                }
                this.loading = false;
            });
        }
    }


}