import { Component } from '@angular/core';
import { CategoryService } from '../../../../service/goal_category.service';
import { Category } from '../../../../model/goal_category.model';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { routerTransition } from '../../../../router.animations';
@Component({

    selector: 'goalcategory-List',
    templateUrl: './goal-category-list-update.component.html',
    providers: [CategoryService],
    animations: [routerTransition()]

})
export class GoalCategoryFormAddUpdateComponent {

    isSave: boolean = true;
    isUpdate: boolean = true;
    isValid: boolean = true;
    isValidErrorMsg: string = "";
    buttonValue: String = "Save";
    id: String = null;
    categoryResults: Category;
    category = new Category();
    loading:boolean =false;
    
    constructor(private categoryService: CategoryService,
        private activatedRoute: ActivatedRoute,
        private router: Router) {
        this.categoryResults = {
            "_id": "",
            "name": ""
        }
    }

    ngOnInit() {
        try {
            this.activatedRoute.params.subscribe(params => {
                this.id = params.id;
            });
        } catch (error) { }
        if (this.id != null) {
            this.isUpdate = true;
            this.isSave = false;
            this.loading = true;
            var res = this.categoryService.getCategoryById(this.id);
            res.subscribe(results => {
                if (results != undefined && results["data"] != undefined) {

                    this.categoryResults = results["data"] as Category;
                }
                this.loading = false;
            });
        } else {
            this.isUpdate = false;
            this.isSave = true;
        }
        if ((this.isSave == true) && (this.isUpdate == false)) {
            this.buttonValue = "Save";
        }
        if ((this.isSave == false) && (this.isUpdate == true)) {
            this.buttonValue = "Update";
        }

    }
    onUpdate(formData) {
        if (this.isSave == true) {
            this.loading = true;
            return this.categoryService.addCategory(formData)
                .subscribe((res) => {
                    if (res != undefined && res.success != undefined) {
                        if (res.success == true) {
                            alert("Added");
                            this.isValid = true;
                            this.redirect();
                        } else {
                            this.isValid = false;
                            this.isValidErrorMsg = res.message;
                        }
                    } else {
                        this.isValid = false;
                        this.isValidErrorMsg = "Error occure please check the data"
                    }
                    this.loading = false;
                });
        }
        else if (this.isUpdate == true) {
            this.loading = true;
            return this.categoryService.updateCategory(formData)
                .subscribe((res) => {
                    if (res != undefined && res.success != undefined) {
                        if (res.success == true) {
                            alert("Updated");
                            this.isValid = true;
                            this.redirect();
                        } else {
                            this.isValid = false;
                            this.isValidErrorMsg = res.message;
                        }
                    } else {
                        this.isValid = false;
                        this.isValidErrorMsg = "Error occure please check the data"
                    }
                    this.loading = false;
                }
                );
        }
    }
    redirect() { this.router.navigate(['/goalcategory-list']); }

    validate() {
        if (this.categoryResults.name != undefined && this.categoryResults.name.toString().trim().length == 0) {
            this.categoryResults.name = "";
        }
    }
}