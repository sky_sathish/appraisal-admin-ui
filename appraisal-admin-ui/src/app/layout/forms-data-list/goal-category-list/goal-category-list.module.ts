import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { GoalsubCategoryListRoutingModule } from './goal-category-list-routing.module';
import { GoalCategoryListComponent } from './goal-category-list.component';
import { PageHeaderModule } from './../../../shared';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { GoalCategoryFormAddUpdateComponent } from './goal-category-list-update/goal-category-list-update.component';
@NgModule({
    imports: [CommonModule, GoalsubCategoryListRoutingModule, PageHeaderModule,FormsModule,Ng2SmartTableModule],
    declarations: [GoalCategoryListComponent,GoalCategoryFormAddUpdateComponent]
})
export class GoalCategoryListModule {}
