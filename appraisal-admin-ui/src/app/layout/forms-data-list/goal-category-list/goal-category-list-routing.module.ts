import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoalCategoryListComponent } from './goal-category-list.component';
import { GoalCategoryFormAddUpdateComponent } from './goal-category-list-update/goal-category-list-update.component';
const routes: Routes = [
    {
        path: '', component: GoalCategoryListComponent
    },
    {
        path: 'form', component: GoalCategoryFormAddUpdateComponent
    },
    {
        path: 'form/:id', component: GoalCategoryFormAddUpdateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GoalsubCategoryListRoutingModule {
}
