import { Component } from '@angular/core';
import { AppraisalMaster } from '../../../../model/appraisal_master.model';
import { AppraisalMasterService } from '../../../../service/appraisal_master.service';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { routerTransition } from '../../../../router.animations';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AppraisalUser } from '../../../../model/appraisal_user.model';
import { EmployeeAppraisal } from '../../../../model/employee.appraisal';
import { AppraisalUserService } from '../../../../service/appraisal_user.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
@Component({

    selector: 'appraisal-master-form',
    templateUrl: './appraisal-master.component.html',
    providers: [AppraisalMasterService, AppraisalUserService],
    animations: [routerTransition()]

})
export class AppraisalMasterAddUpdateComponent {

    isSave: boolean = true;
    isUpdate: boolean = true;
    isValid: boolean = true;
    isValidErrorMsg: string = "";
    isDuplicate: boolean = false;
    buttonValue: String = "Save";
    statusOptions: String[] = ["Active", "Inactive"];
    loading: boolean = false;
    mainComponent: boolean = true;
    id: String = null;
    appraisalMasterResults: AppraisalMaster;
    appraisalMaster = new AppraisalMaster();
    date = new Date();
    appraisalUserResults: AppraisalUser[];

    employeeappraisal = new EmployeeAppraisal();
    closeResult: string;
    myForm: any;

    locked: Boolean = false;
    lockBtn: String = "Lock";
    lockedStatus: String = "";
    lockBtnColor: String = "btn-info";

    alertMsg: string = "Updated";
    managersOptions: IMultiSelectOption[];

    mySettings: IMultiSelectSettings = {
        enableSearch: true,
        checkedStyle: 'fontawesome',
        selectionLimit: 1,
        focusBack: true
    };
    myTexts: IMultiSelectTexts = {
        checkAll: 'Select all',
        uncheckAll: 'Unselect all',
        checked: 'item selected',
        checkedPlural: 'items selected',
        searchPlaceholder: 'Find',
        searchEmptyResult: 'Nothing found...',
        searchNoRenderText: 'Type in search box to see results...',
        defaultTitle: 'Select',
        allSelected: 'All selected',
    };

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private appraisalMasterService: AppraisalMasterService,
        private spinnerService: Ng4LoadingSpinnerService,
        private appraisalUserService: AppraisalUserService,
        private modalService: NgbModal) {

        this.appraisalMasterResults = {
            "_id": "",
            "appraisal_name": "",
            "appraisal_intro":"",
            "created_by": "sathish",
            "created_date": this.date,
            "due_date": this.date,
            "modified_by": "sathish",
            "modified_date": this.date,
            "status": "Active",
            "type": 1,
            "islocked": false
        }
    }

    ngOnInit() {

        try {
            this.activatedRoute.params.subscribe(params => {
                this.id = params.id;
            });
        } catch (error) { }
        if (this.id != null) {
            this.isUpdate = true;
            this.isSave = false;
            this.loading = true;
            var res = this.appraisalMasterService.getAppraisalMasterById(this.id);
            res.subscribe(results => {
                if (results != undefined && results["data"] != undefined) {
                    this.appraisalMasterResults = results["data"] as AppraisalMaster;
                    this.locked = this.appraisalMasterResults.islocked;
                    this.defaultSetup();
                }
                this.loading = false;
            });
            this.LoadUserData(this.id);
            
        } else {
            this.isUpdate = false;
            this.isSave = true;
        }
        if ((this.isSave == true) && (this.isUpdate == false)) {
            this.buttonValue = "Save";
        }
        if ((this.isSave == false) && (this.isUpdate == true)) {
            this.buttonValue = "Update";
        }
    }

    onLoad() {
        this.loading = true;
        this.mainComponent = false;
    }
    onDone() {
        this.loading = false;
        this.mainComponent = true;
    }
    onDateSelect(event) {
        this.appraisalMasterResults.due_date = new Date(event.year + "-" + event.month + "-" + event.day);
    }
    redirect() {
        this.router.navigate(['/appraisal']);
    }
    validate() {
        if (this.appraisalMasterResults.appraisal_name != undefined && this.appraisalMasterResults.appraisal_name.toString().trim().length == 0) {
            this.appraisalMasterResults.appraisal_name = "";
        }
        if (this.appraisalMasterResults.appraisal_intro != undefined && this.appraisalMasterResults.appraisal_intro.toString().trim().length == 0) {
            this.appraisalMasterResults.appraisal_intro = "";
        }
    }
    lock() {
        if (confirm("Are you sure to Lock? \nNote:\nIf you once lock all fields are disabled! ")) {
        this.appraisalMasterResults.islocked = true;
        this.isUpdate = true;
        this.isSave = false;
        this.alertMsg = "Locked"
        this.onAddUpdate();
        }
    }
    defaultSetup() {
        if (this.locked == true) {
            this.lockBtn = "Locked";
            this.lockedStatus = "Locked";
            this.lockBtnColor = "btn-default";
            this.alertMsg = "Locked";
        }
    }
    onDelete(data) {
        if (confirm("Are you sure to delete?")) {
            this.loading = true;
            return this.appraisalUserService.deleteAppraisalUserr(data._id).subscribe(() => {
                var index = this.appraisalUserResults.indexOf(data, 0);
                if (index > -1) {
                    this.appraisalUserResults.splice(index, 1);
                }
                this.loading = false;
            });
        }
    }
    onAddUpdate() {
        if (true) {
            if (this.isSave == true) {
                this.onLoad();
                this.loading = true;
                return this.appraisalMasterService.addAppraisalMaster(this.appraisalMasterResults).subscribe((data) => {
                    if (data != undefined && data.success != undefined) {
                        if (data.success == true) {
                            this.onDone();
                            this.isValid = true;
                            alert("Added");
                            this.LoadUserData(data.result._id);
                        } else {
                            this.onDone();
                            this.isValid = false;
                            this.isValidErrorMsg = data.message
                        }
                    } else {
                        this.onDone();
                        this.isValid = false;
                        this.isValidErrorMsg = "Error occure please check the data 123"
                    }
                    this.loading = false;
                });
            } else if (this.isUpdate == true) {
                this.appraisalMasterResults.modified_date = new Date();
                this.loading = true;
                return this.appraisalMasterService.updateAppraisalMaster(this.appraisalMasterResults).subscribe((data) => {
                    if (data != undefined && data.success != undefined) {
                        if (data.success == true) {
                            alert(this.alertMsg);
                            this.isValid = true;
                            this.redirect();
                            this.defaultSetup();
                        } else {
                            this.isValid = false;
                            this.isValidErrorMsg = data.message
                        }
                    } else {
                        this.isValid = false;
                        this.isValidErrorMsg = "Error occure please check the data"
                    }
                    this.loading = false;
                });
            }
        }// if false
    }
    onUpdate(formList) {
        //  console.log(JSON.stringify(formList))
        var filteredArrayList = [];
        for (let list of formList) {
            filteredArrayList.push({ "_id": list._id, "appraiser_id": list.appraiser_id, "reviewer_id": list.reviewer_id });
        }
        try {
            this.onLoad();
            this.loading = true;
            var res = this.appraisalUserService.updateAppraisalUserBulkList(filteredArrayList).subscribe((data) => {
                if (data != undefined && data.success != undefined) {
                    if (data.success == true) {
                        alert("Updated users list");
                        this.isValid = true;
                        this.onDone();
                    } else {
                        this.isValid = false;
                        this.isValidErrorMsg = data.message
                        this.onDone();
                    }
                }
                this.loading = false;
            });
        } catch (error) {
        }
    }
    checkBoxEvent(data, event) {
        if (event.target.checked) {
            alert("Activated")
            data.status = "Active";
        } else {
            alert("Inactivated")
            data.status = "Inactive";
        }

        try {
            var res = this.appraisalUserService.updateAppraisalUser(data).subscribe((data) => {
                if (data != undefined && data.success != undefined) {
                    if (data.success == true) {
                        alert("Updated");
                        this.isValid = true;
                    } else {
                        this.isValid = false;
                        this.isValidErrorMsg = data.message
                    }
                }
            });
        } catch (error) {
        }
    }
    LoadUserData(id) {
        var res = this.appraisalUserService.getAppraisalById(id);
        res.subscribe(results => {
            if (results != undefined && results["data"] != undefined) {
                this.appraisalUserResults = results["data"] as AppraisalUser[];
                this.managersOptions = [];
                this.appraisalUserResults.forEach(ele => {
                    this.managersOptions.push({ "id": ele.hash, "name": ele.first_name + " " + ele.last_name })
                })
            }
        });
    }
    onModelOpen(content, appraisalUser) {
        this.employeeappraisal = {
            "appraisal_id": appraisalUser.appraisal_id,
            "employee_id": appraisalUser.hash,
            "employee_name": appraisalUser.first_name + " " + appraisalUser.last_name,
            "employee_code": appraisalUser.emp_code,
            "designation": appraisalUser.designation,
            "supervisor": appraisalUser.supervisor_name,
            "appraiser_name": appraisalUser.appraiser_name,
            "appraiser_id": appraisalUser.appraiser_id,
            "reviewer_name": appraisalUser.reviewer_name,
            "reviewer_id": appraisalUser.reviewer_id

        };
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }
    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
    onUpdateModel(formData) {
        this.managersOptions.forEach(ele => {
            if (ele.id == formData.appraiser_id) {
                this.employeeappraisal.appraiser_id = ele.id;
                this.employeeappraisal.appraiser_name = ele.name;
            }
            if (ele.id == formData.reviewer_id) {
                this.employeeappraisal.reviewer_id = ele.id;
                this.employeeappraisal.reviewer_name = ele.name;
            }
        })
        try {
            var res = this.appraisalUserService.updateEmployee(this.employeeappraisal).subscribe((data) => {
                if (data != undefined && data.success != undefined) {
                    if (data.success == true) {
                        this.isValid = true;
                        this.LoadUserData(data.result);
                        alert("Updated");
                    } else {
                        this.isValid = false;
                        this.isValidErrorMsg = data.message
                    }
                }
            });
        } catch (error) {
            alert("error")
        }
    }
}