import { Component, ViewChild, ElementRef } from '@angular/core';
import { AppraisalUserService } from '../../../../service/appraisal_user.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({

    selector: 'appraisee-status',
    templateUrl: './appraisee-status.component.html',
    providers: [AppraisalUserService],
    animations: [],
    styleUrls: ['./appraisee-status.component.scss']

})
export class AppraiseeStatusComponent {

    mainComponent: boolean = true;
    loading = false;
    showTable = true;
    dataNotFound = false;
    public id: String = null;
    empResult: String[] = [];
    empFilteredResult: any = [{}];
    tableHeaderLabel:String ="";
    perPageRecord = 5;
    // Doughnut
    public doughnutChartLabels: string[] = [
        'Appraisal Started',
        'Appraisal In-Review',
        'Appraisal Reviewed',
        'Appraisal Disagreed',
        'Appraisal Completed'
    ];
    public doughnutChartData: number[] = [10, 10, 10, 101, 10];
    public doughnutChartType: string = 'doughnut';
    public chartColors: any[] = [
        {
            backgroundColor: ["#ffc107", "#17a2b8", "#0069d9", "#dc3545", "#28a745"]
        }
    ];

    // smart table (documentaion link:https://akveo.github.io/ng2-smart-table/#/documentation)
    settings = {
        columns: {
            seq: {
            title: 'ID',
            editable : false,
            width: '20%'
          },
          name: {
            title: 'Full Name',
            editable : false,
            width: '30%'
          },
          emp_code: {
            title: 'Code',
            editable : false,
            width: '20%'
          },
          supervisor: {
            title: 'Supervisor',
            editable : false,
            width: '30%'
          }
        },
        mode: 'external',//'inline'(can edit user side) | 'external'(can not be edit user side)
        hideHeader : false,
        hideSubHeader: false,// eg.search field
        noDataMessage: "Data not found!!!",
        actions:{
            columnTitle: "Actions",//default value is 'Actions'
            add:false,
            edit:false,
            delete:false,
            position:'left',//|'right'
        },
        pager: {
            display:true,
            perPage:this.perPageRecord
        }
      };
   
    // smart table end
    constructor(
        private appraisalUserService: AppraisalUserService,
        private activatedRoute: ActivatedRoute) { }

    ngOnInit() {

        try {
            this.activatedRoute.params.subscribe(params => {
                this.id = params.id;
            });
        } catch (error) { }

        if (this.id != null) {
            var res = this.appraisalUserService.getAppraisalByAppraisalId(this.id);
            res.subscribe(results => {
                if (results != undefined && results["result"] != undefined) {
                    // console.log(JSON.stringify(results))
                    this.doughnutChartData = [
                        results["result"]["started"],
                        results["result"]["submitted"],
                        results["result"]["reviewed"],
                        results["result"]["disagree"],
                        results["result"]["agree"]
                    ];
                    if (results["result"]["data"] != undefined) {
                        for (let ele of results["result"]["data"]) {
                            this.empResult.push(ele);
                        }
                    }
                }// if condition end
                if(this.empResult.length == 0){
                    this.dataNotFound = true;
                }
            }// subscribe end
        );
        }
    }

    @ViewChild('empDetails', { read: ElementRef }) public panel: ElementRef<any>;
    @ViewChild('empChart', { read: ElementRef }) public empChart: ElementRef<any>;
    chartClicked(event) {
        this.empFilteredResult = [];
        var showStatus = "";
        if (event["active"] != undefined &&
            event["active"]["0"] != undefined &&
            event["active"]["0"]["_view"] != undefined &&
            event["active"]["0"]["_view"]["backgroundColor"] != undefined) {
            if (event["active"]["0"]["_view"]["backgroundColor"] == "rgb(235, 176, 0)") {
                showStatus = "started"
            }
            if (event["active"]["0"]["_view"]["backgroundColor"] == "rgb(0, 164, 189)") {
                showStatus = "submitted"
            }
            if (event["active"]["0"]["_view"]["backgroundColor"] == "rgb(0, 96, 199)") {
                showStatus = "reviewed"
            }
            if (event["active"]["0"]["_view"]["backgroundColor"] == "rgb(250, 0, 25)") {
                showStatus = "disagree"
            }
            if (event["active"]["0"]["_view"]["backgroundColor"] == "rgb(8, 181, 48)") {
                showStatus = "agree"
            }
            var count=0;
            for (let ele of this.empResult) {
                if (ele["status"] == showStatus){
                count = count+1;
                    this.empFilteredResult.push(
                        {
                            "seq":count,
                            "name": ele["name"],
                            "emp_code": ele["emp_code"],
                            "supervisor": ele["supervisor"]
                        })
                    }
            }
            // employee data table header information
            if(this.empFilteredResult.length > 0){
                if(showStatus == "started"){ this.tableHeaderLabel = "Still couldn't start appraisee's : "+this.doughnutChartData[0];}
                if(showStatus == "submitted"){ this.tableHeaderLabel = "Total Submitted appraisee's : "+this.doughnutChartData[1];}
                if(showStatus == "reviewed"){ this.tableHeaderLabel = "Total Reviewed appraisee's : "+this.doughnutChartData[2];}
                if(showStatus == "disagree"){ this.tableHeaderLabel = "Total Disagreed appraisee's : "+this.doughnutChartData[3];}
                if(showStatus == "agree"){ this.tableHeaderLabel = "Total Agreed appraisee's : "+this.doughnutChartData[4];}
               
                this.panel.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'start' });
                this.showTable = false;
            }
        }else{
            this.showTable = true;
            this.tableHeaderLabel = "Please Selecte chart!!!";
        }
        
        

        
    }

    // dropDown call
    changeRecordRows(val){
        this.perPageRecord = val;
    }

}