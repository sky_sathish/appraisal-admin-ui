import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { AppraisalMasterRoutingModule } from './appraisal-master-list-routing.module';
import { AppraisalMasterListComponent } from './appraisal-master-list.component';
import { AppraisalMasterAddUpdateComponent } from './appraisal-master-add-update-form/appraisal-master.component';
import { PageHeaderModule } from './../../../shared';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { AppraiseeStatusComponent } from './appraisee-status/appraisee-status.component';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {DataTableModule} from "angular-6-datatable";
import { Ng2SmartTableModule } from 'ng2-smart-table';
@NgModule({
    imports: [
        CommonModule, 
        AppraisalMasterRoutingModule, 
        PageHeaderModule,
        FormsModule,
        Ng4LoadingSpinnerModule.forRoot(),
        NgbModule.forRoot(),
        MultiselectDropdownModule,
        Ng2Charts,
        DataTableModule,
        Ng2SmartTableModule
    ],


    declarations: [
        AppraisalMasterListComponent,
        AppraisalMasterAddUpdateComponent,
        AppraiseeStatusComponent
    ]
})
export class AppraisalMasterListModule {}
