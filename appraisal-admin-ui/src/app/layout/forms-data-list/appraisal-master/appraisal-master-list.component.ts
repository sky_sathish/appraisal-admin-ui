import { Component } from '@angular/core';
import { AppraisalMaster } from '../../../model/appraisal_master.model';
import { AppraisalMasterService } from '../../../service/appraisal_master.service';
import { LocalDataSource } from 'ng2-smart-table';
@Component({
  selector: 'appraisal-master',
  templateUrl: './appraisal-master-list.component.html',
  providers: [AppraisalMasterService]
})
export class AppraisalMasterListComponent {
  appraisalMasterResultsTest:any =[];
  model: any;
  appraisalMasterResults: AppraisalMaster[];
  selectedRows;
  emptyMsg: string = 'loading...';
  switchOnOff = false;
  perPageRecord = 5;
  showTable =false;
  source: LocalDataSource;
  loading:boolean =false;

  constructor(private appraisalMasterService: AppraisalMasterService) {
    
   }
 

  // smart table (documentaion link:https://akveo.github.io/ng2-smart-table/#/documentation)
  settings = {
    columns: {
      seq:{
        title: 'No.',
        editable: false,
        width: '10%'
      },
      appraisal_name: {
        title: 'Appraisal Name',
        editable: false,
        width: '90%'
      }
    },
    mode: 'external',//'inline'(can edit user-side) | 'external'(can not be edit user-side)
    hideHeader: false,
    hideSubHeader: false,// eg.search field
    noDataMessage: "Data not found!!!",
    actions: {
      add: false,
      edit: false,
      delete: false,
      custom: [
      { name: 'editCall', title: '<i class="fa fa-edit"></i>&nbsp;' },
      { name: 'deleteCall', title: '<i class="fa fa-trash"></i>&nbsp;' },
      { name: 'statusCall', title: '<i class="fa fa-tasks"></i>&nbsp;' }
    ],
      
    },
    pager: {
      display: true,
      perPage: this.perPageRecord
    }
  };

  
  ngOnInit() {
    this.loading = true; 
    this.source = new LocalDataSource();
    this.appraisalMasterService.getAppraisalMaster().then(result =>{
      
      if (result != undefined ) {
        this.appraisalMasterResults = result as AppraisalMaster[];
        var aprData = [];
        if (this.appraisalMasterResults != undefined && this.appraisalMasterResults.length > 0) {
            var count=0;
             this.appraisalMasterResults.forEach(ele=>{
              count = count+1;
              aprData.push(
                {
                  "seq":count,
                  "id" :ele._id,
                  "appraisal_name":ele.appraisal_name,
                  "action":ele._id
                }
              )
            });
            this.showTable =true;
            this.source.load(aprData);
        }
      }
      this.loading = false;
    });
   }
  onCustomAction(event) {

    if (event["action"] != undefined) {
        var id = null;
        id = event["data"]["id"];
        if (event["action"] == "statusCall" && id !=null && id !=undefined) {
          window.location.href = "/appraisal/status/" + id;
      }
        if (event["action"] == "editCall" && id !=null && id !=undefined) {
            window.location.href = "/appraisal/form/" + id;
        }
        if (event["action"] == "deleteCall" && id !=null && id !=undefined) {
            if (confirm("Are you sure to delete?")) {
                return this.appraisalMasterService.deleteAppraisalMasterr(id).then(result => {
                    this.source.remove(event["data"]).then(ele=>{
                     });
                });
            }

        }
    }
}
  onLoadData(){
    this.loading = true;
    return this.appraisalMasterService.getAppraisalMaster().then(result =>{
      
      // console.log(JSON.stringify(result));
      if (result != undefined ) {
        this.appraisalMasterResults = result as AppraisalMaster[];
        var aprData = [];
        if (this.appraisalMasterResults != undefined && this.appraisalMasterResults.length > 0) {
          console.log("=================================")  
          // console.log(JSON.stringify(this.appraisalMasterResults))  
            var count=0;
             this.appraisalMasterResults.forEach(ele=>{
              count = count+1;
              
              aprData.push(
                {
                  "seq":count,
                  "id" :ele._id,
                  "appraisal_name":ele.appraisal_name,
                  "action":ele._id
                }
              )
            });
            this.showTable =true;
            // console.log(JSON.stringify(aprData))  
            return aprData;
        }
      }
      this.loading = false;
    });
  }
  onDelete(data) {
    if (confirm("Are you sure to delete?")) {
      return this.appraisalMasterService.deleteAppraisalMasterr(data._id).subscribe(() => {

        var index = this.appraisalMasterResults.indexOf(data, 0);
        if (index > -1) {
          this.appraisalMasterResults.splice(index, 1);
        }
      });
    }
  }
  switchTable(){
    if(this.switchOnOff == true){
        this.switchOnOff = false;
    }else{
    this.switchOnOff = true;
    }
}
  onAppraiseeStatus(data) {
    alert("status")
  }

  changeSort(event) {
    console.log('sort', event);
  };

  rowClick(event) {
    console.log('click', event);
  }

  rowSelect(event) {
    console.log('select', event);
  }

  rowUnselect(event) {
    console.log('unselect', event);
  }

  onSelectionChange(event) {
    console.log('selectionChange', event);
  }

  flagHeaderClick(col) {
    console.log('flagHeaderClick ==>', col);
  }

  flagClicked(event, row) {
    console.log('flagClicked ==>', row);
  }

  rowExpanded(event) {
    console.log('expanded', event);
  }

  rowCollapse(event) {
    console.log('collapse', event);
  }

  editInit(event) {
    console.log('edit Init');
    console.log(event);
  }

  editComplete(event) {
    console.log('edit complete')
    console.log(event);
  }

  edit(event) {
    console.log('Edit');
    console.log(event);
  }

  editCancel(event) {
    console.log('edit cancel');
    console.log(event);
  }

  valueChange(event) {
    console.log('valueChange', event);
  }

  onFilter(val) {
    console.log('filter change', val);
    if (!val.length)
      this.emptyMsg = 'No data found';
    else
      this.emptyMsg = '';
  }

  tableReload() {
    console.log('reload logic');
  }

  onPage(val) {
    console.log('pagechange', val);
  }
}