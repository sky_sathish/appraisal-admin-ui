import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppraisalMasterListComponent } from './appraisal-master-list.component';
import { AppraisalMasterAddUpdateComponent } from './appraisal-master-add-update-form/appraisal-master.component';
import { AppraiseeStatusComponent } from './appraisee-status/appraisee-status.component';

const routes: Routes = [
    {
        path: '', component: AppraisalMasterListComponent
    },
    {
        path: 'form', component: AppraisalMasterAddUpdateComponent
    },
    {
        path: 'form/:id', component: AppraisalMasterAddUpdateComponent
    },
    {
        path: 'status/:id', component: AppraiseeStatusComponent
    } 
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AppraisalMasterRoutingModule {
}
