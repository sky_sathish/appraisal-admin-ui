import { Component } from '@angular/core';
import { SubcategoryService } from '../../../service/goal_subcategory.service';
import { Subcategory } from '../../../model/goal_subcategory.model';
import { routerTransition } from '../../../router.animations';
import { LocalDataSource } from 'ng2-smart-table';
@Component({

    selector: 'goalsubcategory-List',
    templateUrl: './goal-subcategory-list.component.html',
    providers: [SubcategoryService],
    animations: [routerTransition()]


})
export class GoalSubCategoryListComponent {

    subcategoryResults: Subcategory[];
    switchOnOff = false;
    perPageRecord = 5;
    source: LocalDataSource;
    loading: boolean = false;
    // smart table (documentaion link:https://akveo.github.io/ng2-smart-table/#/documentation)
    settings = {
        columns: {
            seq: {
                title: 'No.',
                editable: false,
                width: '10%'
            },
            name: {
                title: 'Goal Subcategory Name',
                editable: false,
                width: '90%'
            }
        },
        mode: 'external',//'inline'(can edit user-side) | 'external'(can not be edit user-side)
        hideHeader: false,
        hideSubHeader: false,// eg.search field
        noDataMessage: "Data not found!!!",
        actions: {
            add: false,
            edit: false,
            delete: false,
            custom: [
                { name: 'editCall', title: '<i class="fa fa-edit"></i>&nbsp;' },
                { name: 'deleteCall', title: '<i class="fa fa-trash"></i>&nbsp;' }
            ]

        },
        pager: {
            display: true,
            perPage: this.perPageRecord
        }
    };
    constructor(private subcategoryService: SubcategoryService) {

    }

    ngOnInit() {

        this.source = new LocalDataSource();
        this.loading = true;
        this.subcategoryService.getSubcategory()
            .subscribe(results => {
                if (results != undefined && results["data"] != undefined) {
                    this.subcategoryResults = results["data"]["docs"] as Subcategory[];
                    var loadData: any = [];
                    var count = 0;
                    for (var i = 0; i < this.subcategoryResults.length; i++) {
                        if (this.subcategoryResults[i] != undefined && this.subcategoryResults[i] != null) {
                            count = count + 1;
                            loadData.push({
                                "seq": count,
                                "name": this.subcategoryResults[i]["name"],
                                "id": this.subcategoryResults[i]["_id"]
                            })
                        }
                    }
                    this.source.load(loadData);
                }
                this.loading = false;
            });
    }

    onCustomAction(event) {

        if (event["action"] != undefined) {
            var id = null;
            id = event["data"]["id"];

            if (event["action"] == "editCall" && id != null && id != undefined) {
                window.location.href = "/goalcategory-list/form/" + id;
            }
            if (event["action"] == "deleteCall" && id != null && id != undefined) {
                if (confirm("Are you sure to delete?")) {
                    return this.subcategoryService.deleteSubcategory(id).then(result => {
                        this.source.remove(event["data"]).then(ele => {
                        });
                    });
                }

            }
        }
    }
    switchTable() {
        if (this.switchOnOff == true) {
            this.switchOnOff = false;
        } else {
            this.switchOnOff = true;
        }
    }
    onDelete(data) {
        if (confirm("Are you sure to delete?")) {
            this.loading = true;
            return this.subcategoryService.deleteSubcategory(data._id).then(() => {
                var index = this.subcategoryResults.indexOf(data, 0);
                if (index > -1) {
                    this.subcategoryResults.splice(index, 1);
                }
                this.loading = false;
            });
        }
    }
}