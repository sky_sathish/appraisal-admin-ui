import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { GoalsubCategoryListRoutingModule } from './goal-subcategory-list-routing.module';
import { GoalSubCategoryListComponent } from './goal-subcategory-list.component';
import { PageHeaderModule } from './../../../shared';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { GoalSubCategoryFormAddUpdateComponent } from './goal-subcategory-form-update/goal-subcategory-update.component';
@NgModule({
    imports: [CommonModule, GoalsubCategoryListRoutingModule, PageHeaderModule,FormsModule,Ng2SmartTableModule],
    declarations: [GoalSubCategoryListComponent,GoalSubCategoryFormAddUpdateComponent]
})
export class GoalSubCategoryListModule {}
