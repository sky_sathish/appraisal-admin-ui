import { Component } from '@angular/core';
import { SubcategoryService } from '../../../../service/goal_subcategory.service';
import { Subcategory } from '../../../../model/goal_subcategory.model';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { routerTransition } from '../../../../router.animations';
@Component({

    selector: 'goalsubcategory-List',
    templateUrl: './goal-subcategory-update.component.html',
    providers: [SubcategoryService],
    animations: [routerTransition()]

})
export class GoalSubCategoryFormAddUpdateComponent {

    isSave: boolean = true;
    isUpdate: boolean = true;
    buttonValue: String = "Save";
    id: String = null;
    subcategoryResults: Subcategory;
    subcategory = new Subcategory();
    loading: boolean = false;
    constructor(private subcategoryService: SubcategoryService,
        private activatedRoute: ActivatedRoute,
        private router: Router) {
        this.subcategoryResults = {
            "_id": "",
            "name": ""
        }
    }

    ngOnInit() {
        try {
            this.activatedRoute.params.subscribe(params => {
                this.id = params.id;
            });
        } catch (error) { }
        if (this.id != null) {
            this.isUpdate = true;
            this.isSave = false;
            this.loading = true;
            this.subcategoryService.getSubcategoryById(this.id)
                .subscribe(results => {
                    console.log("results : " + results);
                    if (results != undefined && results["data"] != undefined) {

                        this.subcategoryResults = results["data"] as Subcategory;

                    }
                    this.loading = false;
                });
        } else {
            this.isUpdate = false;
            this.isSave = true;
        }


        if ((this.isSave == true) && (this.isUpdate == false)) {
            this.buttonValue = "Save";
        }
        if ((this.isSave == false) && (this.isUpdate == true)) {
            this.buttonValue = "Update";
        }
    }
    onUpdate(formData) {
        if (this.isSave == true) {
            this.loading = true;
            return this.subcategoryService.addSubcategory(formData).subscribe(() => {
                alert("Added");
                this.loading = false;
                this.redirect();
            });
        }
        else if (this.isUpdate == true) {
            this.loading = true;
            return this.subcategoryService.updateSubcategory(formData).subscribe(() => {
                alert("updated");
                this.loading = false;
                this.redirect();
            });
        }
    }
    redirect() { this.router.navigate(['/goalsubcategory-list']); }

    validate() {
        if (this.subcategoryResults.name != undefined && this.subcategoryResults.name.toString().trim().length == 0) {
            this.subcategoryResults.name = "";
        }
    }
}