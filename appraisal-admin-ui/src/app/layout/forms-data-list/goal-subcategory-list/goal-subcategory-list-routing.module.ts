import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoalSubCategoryListComponent } from './goal-subcategory-list.component';
import { GoalSubCategoryFormAddUpdateComponent } from './goal-subcategory-form-update/goal-subcategory-update.component';
const routes: Routes = [
    {
        path: '', component: GoalSubCategoryListComponent
    },
    {
        path: 'form', component: GoalSubCategoryFormAddUpdateComponent
    },
    {
        path: 'form/:id', component: GoalSubCategoryFormAddUpdateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GoalsubCategoryListRoutingModule {
}
