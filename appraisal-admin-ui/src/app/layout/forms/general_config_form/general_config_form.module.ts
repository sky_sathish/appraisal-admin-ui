import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { GeneralConfigFormRoutingModule } from './general_config_form-routing.module';
import { GeneralConfigFormComponent } from './general_config_form.component';
import { PageHeaderModule } from './../../../shared';

@NgModule({
    imports: [CommonModule, GeneralConfigFormRoutingModule, PageHeaderModule,FormsModule],
    declarations: [GeneralConfigFormComponent]
})
export class GeneralConfigFormModule {}
