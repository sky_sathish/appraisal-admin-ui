import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralConfigFormComponent } from './general_config_form.component';

const routes: Routes = [
    {
        path: '', component: GeneralConfigFormComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GeneralConfigFormRoutingModule {
}
