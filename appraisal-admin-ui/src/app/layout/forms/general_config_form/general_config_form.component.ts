import { Component } from '@angular/core';
import {NgForm} from '@angular/forms';
import {GeneralConfigService} from '../../../service/general_config.service';
import{GeneralConfig} from '../../../model/general_config.model';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';
@Component({

    selector : 'general-config',
    templateUrl: './general_config_form.component.html',
    providers:[GeneralConfigService]
  
})
export class GeneralConfigFormComponent{

    generalConfig = new GeneralConfig();
    constructor(private generalConfigService:GeneralConfigService){}
    
    onSubmitGeneralConfigAttr(f: NgForm) {
        console.log(f.value);
        this.generalConfigService.addGeneralConfig(this.generalConfig)
        .subscribe(() => {alert("done")});
      }
    
    
}