import { Component } from '@angular/core';
import {NgForm} from '@angular/forms';
import {CompetenciesAttributeService} from '../../../service/competencies_attributes.service';
import{CompetenciesAttribute} from '../../../model/competencies_attribute.model';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';

@Component({

    selector : 'competencies-attribute',
    templateUrl: './competencies_attribute.componenet.html',
    providers:[CompetenciesAttributeService]
  
})
export class CompetenciesAttributeFormComponent{

    
    constructor(private competenciesAttributeService:CompetenciesAttributeService){}
    competenciesAttribute = new CompetenciesAttribute();
 
 
    onSubmitCompetenciesAttr(f: NgForm) {
        console.log(f.value);
        this.competenciesAttributeService.addCompetenciesAttribute(this.competenciesAttribute)
        .subscribe(() => {alert("done")});
      }
    
}