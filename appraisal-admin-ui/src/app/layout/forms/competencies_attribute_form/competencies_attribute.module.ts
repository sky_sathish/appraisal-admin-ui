import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { CompetenciesAttributeRoutingModule } from './competencies_attribute-routing.module';
import { CompetenciesAttributeFormComponent } from './competencies_attribute.componenet';
import { PageHeaderModule } from './../../../shared';

@NgModule({
    imports: [CommonModule, CompetenciesAttributeRoutingModule, PageHeaderModule,FormsModule],
    declarations: [CompetenciesAttributeFormComponent]
})
export class CompetenciesAttributeFunModule {}
