import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompetenciesAttributeFormComponent } from './competencies_attribute.componenet';

const routes: Routes = [
    {
        path: '', component: CompetenciesAttributeFormComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CompetenciesAttributeRoutingModule {
}
