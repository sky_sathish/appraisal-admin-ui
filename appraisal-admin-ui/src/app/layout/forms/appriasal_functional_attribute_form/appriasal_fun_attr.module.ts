import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { AppraisalFunRoutingModule } from './appriasal_fun_attr-routing.module';
import { AppriasalFunctionalAttributeFormComponent } from './appriasal_fun_attr_form.component';
import { PageHeaderModule } from './../../../shared';

import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
@NgModule({
    imports: [CommonModule, AppraisalFunRoutingModule, PageHeaderModule,FormsModule,MultiselectDropdownModule],
    declarations: [AppriasalFunctionalAttributeFormComponent]
})
export class AppraisalFunModule {}
