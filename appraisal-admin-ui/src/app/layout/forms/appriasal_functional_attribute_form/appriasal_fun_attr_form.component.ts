import { Component,OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { IMultiSelectOption,IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import {ApprisalFunctionalAttributeService} from '../../../service/appraisal_functional_attributes.service';
import {ApprisalFunctionalAttribute} from '../../../model/appraisal_functional_attributes.model';
import {Role} from '../../../model/role.model';
import { NgModule } from '@angular/core';
import {HomeService} from '../../../service/home.service';
import {Router} from '@angular/router';
import {Category} from '../../../model/goal_category.model';
import {Subcategory} from '../../../model/goal_subcategory.model';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';
import { FunctionalIDService } from '../../../service/functional_id.service';
@Component({
    selector: 'appriasal-attribute',
    templateUrl: './appriasal_fun_attr_form.component.html',
    providers:[ApprisalFunctionalAttributeService,HomeService,
      FunctionalIDService]
  })

  export class AppriasalFunctionalAttributeFormComponent implements OnInit{

    apprisalFunctionalAttribute = new ApprisalFunctionalAttribute()
     public goalSubCategoryOptions: IMultiSelectOption[];
     functionids: String[];

    public roles:Role[];
    public categorys:Category[];
    public subcategory:Subcategory[]

    //https://www.npmjs.com/package/angular2-dropdown-multiselect
    mySettings: IMultiSelectSettings = {
      enableSearch: true,
      checkedStyle: 'fontawesome',
      dynamicTitleMaxItems: 1,
      displayAllSelectedText: true,
      selectionLimit :1
  };

    constructor(private apprisalFunctionalAttributeService:ApprisalFunctionalAttributeService, 
                private homeService:HomeService,private router:Router,
                private functionalIDService: FunctionalIDService) {} 

    ngOnInit() {

      var req = this.homeService.getHomeDeails();
      req.subscribe(results => {
        
      if(results!=undefined && results["data"]!=undefined &&results["data"]!=undefined){
        this.categorys = results["data"].goalCategory.docs as Category[];
        this.subcategory=results["data"].goalSubcategory.docs as Subcategory[];

        this.goalSubCategoryOptions = [];
        for (let entry of this.subcategory) {
          this.goalSubCategoryOptions.push({id: ""+entry._id, name: ""+entry.name});
        }
      }
      if(results!=undefined&&results["data"]!=undefined && results["data"].role!=undefined&& results["data"].role.docs!=undefined ){
            this.roles=results["data"].role.docs as Role[] ;
      }
      });

      this.functionids = this.functionalIDService.getFunctionalID();

      this.apprisalFunctionalAttribute.target_for_last_year_unit = "%";
      this.apprisalFunctionalAttribute.target_for_next_year_unit = "%";
        
    }
    onSubmitAppraisalFuncAttr(f: NgForm) {
      if(f.value.goal_subcategory[0] !=undefined && f.value.goal_subcategory[0] != ''){
        this.apprisalFunctionalAttribute.subcategory = f.value.goal_subcategory[0];
      }
        this.apprisalFunctionalAttributeService.addApprisalFunctionalAttribute(this.apprisalFunctionalAttribute)
        .subscribe(() => {alert("done");this.redirect();});
      }
      validate(){    
        if(this.apprisalFunctionalAttribute.target_for_last_year!=undefined && this.apprisalFunctionalAttribute.target_for_last_year.toString().trim().length==0){
          this.apprisalFunctionalAttribute.target_for_last_year="";
        }
        if(this.apprisalFunctionalAttribute.target_for_last_year_unit!=undefined && this.apprisalFunctionalAttribute.target_for_last_year_unit.toString().trim().length==0){
          this.apprisalFunctionalAttribute.target_for_last_year_unit="";
        }
        if(this.apprisalFunctionalAttribute.target_for_next_year!=undefined && this.apprisalFunctionalAttribute.target_for_next_year.toString().trim().length==0){
          this.apprisalFunctionalAttribute.target_for_next_year="";
        }
        if(this.apprisalFunctionalAttribute.target_for_next_year_unit!=undefined && this.apprisalFunctionalAttribute.target_for_next_year_unit.toString().trim().length==0){
          this.apprisalFunctionalAttribute.target_for_next_year_unit="";
        }
        if(this.apprisalFunctionalAttribute.weightage!=undefined && this.apprisalFunctionalAttribute.weightage.toString().trim().length==0){
          // this.apprisalFunctionalAttribute.weightage=0;
        }
      }
      redirect() {this.router.navigate(['/apprisal-functional-attribute-list']); }
  
  }
