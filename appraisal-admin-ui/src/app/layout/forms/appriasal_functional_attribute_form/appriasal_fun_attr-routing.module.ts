import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppriasalFunctionalAttributeFormComponent } from './appriasal_fun_attr_form.component';

const routes: Routes = [
    {
        path: '', component: AppriasalFunctionalAttributeFormComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AppraisalFunRoutingModule {
}
