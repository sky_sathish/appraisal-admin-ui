import { Component,OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {CompetenciesFunctionalAttributesService} from '../../../service/competencies_functional_attributes.service';
import{CompetenciesFunctionalAttributes} from '../../../model/competencies_functional_attributes.model';
import {HomeService} from '../../../service/home.service';
import {Role} from '../../../model/role.model';
import {CompetenciesLevelService} from '../../../service/competencies_level.service';
import {CompetenciesLevel} from '../../../model/competencies_level.model';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';
import { FunctionalIDService } from '../../../service/functional_id.service';
import {Router} from '@angular/router';
@Component({
    selector: 'competenciesfun-attribute',
    templateUrl: './competencies_functional_attribute.component.html',
    providers:[CompetenciesFunctionalAttributesService,
        HomeService,CompetenciesLevelService,
        FunctionalIDService]
  })
  export class CompetenciesFunctionalAttributeFormComponent implements OnInit{

    competenciesFunctionalAttributes = new CompetenciesFunctionalAttributes();
    functionids:String[];
    public roles:Role[];
    public competenciesLevelResults:CompetenciesLevel[];
    constructor(private competenciesFunctionalAttributesService:CompetenciesFunctionalAttributesService,
        private homeService:HomeService,
        private competenciesLevelService:CompetenciesLevelService,
        private functionalIDService:FunctionalIDService,
        private router:Router) {} 

    ngOnInit() {
      this.competenciesFunctionalAttributes.weightage_unit="%";
      var req = this.homeService.getHomeDeails();
      req.subscribe(results => { 
        console.log( results);
          if(results!=undefined&&results["data"]!=undefined && results["data"].role!=undefined&& results["data"].role.docs!=undefined ){
            this.roles=results["data"].role.docs as Role[] ;
        }
        });

        var res = this.competenciesLevelService.getCompetenciesLevel();
        res.subscribe(results=>{
        console.log("results : "+results);
        if(results!=undefined&&results["data"]!=undefined){

            this.competenciesLevelResults=results["data"]["docs"] as CompetenciesLevel[] ;
            }
    });

    this.functionids = this.functionalIDService.getFunctionalID();
        
    }
    onSubmitCompetenciesFuncAttr(f: NgForm) {
        console.log(f.value);
        this.competenciesFunctionalAttributesService.
        addCompetenciesFunctionalAttributes(this.competenciesFunctionalAttributes)
        .subscribe(() => {alert("done");this.redirect()});
      }

      redirect() {this.router.navigate(['/competencies-functional-attributes-list']); }
      validate(){    
        if(this.competenciesFunctionalAttributes.weightage_unit!=undefined && this.competenciesFunctionalAttributes.weightage_unit.toString().trim().length==0){
           this.competenciesFunctionalAttributes.weightage_unit="";
        }
      }
  }
