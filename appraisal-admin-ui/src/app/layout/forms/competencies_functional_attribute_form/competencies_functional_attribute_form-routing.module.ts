import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompetenciesFunctionalAttributeFormComponent } from './competencies_functional_attribute.component';

const routes: Routes = [
    {
        path: '', component: CompetenciesFunctionalAttributeFormComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CompetenciesFunctionalAttributeFormRoutingModule {
}
