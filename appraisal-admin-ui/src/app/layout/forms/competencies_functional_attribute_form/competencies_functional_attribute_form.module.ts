import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { CompetenciesFunctionalAttributeFormRoutingModule } from './competencies_functional_attribute_form-routing.module';
import { CompetenciesFunctionalAttributeFormComponent } from './competencies_functional_attribute.component';
import { PageHeaderModule } from './../../../shared';

@NgModule({
    imports: [CommonModule, CompetenciesFunctionalAttributeFormRoutingModule, PageHeaderModule,FormsModule],
    declarations: [CompetenciesFunctionalAttributeFormComponent]
})
export class CompetenciesFunctionalAttributeFormModule {}
