import { Component,OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {GoalAttributeService} from '../../../service/goal_attribute.service';
import {GoalAttribute} from '../../../model/goal_attribute.model';
import {Category} from '../../../model/goal_category.model';
import {Subcategory} from '../../../model/goal_subcategory.model';
import {HomeService} from '../../../service/home.service';
import { NgModule } from '@angular/core';
import {Router} from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';
@Component({
    selector: 'goal-attribute',
    templateUrl: './goal_attribute.component.html',
    providers:[GoalAttributeService,HomeService]
  })

  export class GoalAttributeComponent implements OnInit{

    goalAttribute= new GoalAttribute();
    category=new Category();
    subcategory=new Subcategory();
    
    public categories:Category[]
    public subcategories:Subcategory[]
    constructor(private goalAttributeService:GoalAttributeService, 
                private homeService:HomeService,
                private router:Router) {} 

  selectedCatValue = "Productivity";
    ngOnInit() {
      
      var req = this.homeService.getHomeDeails();
      req.subscribe(results => { 
        console.log( results);
          if(results!=undefined&&results["data"]!=undefined && results["data"].role!=undefined&& results["data"].role.docs!=undefined ){
            this.categories=results["data"].goalCategory.docs as Category[] ;
            this.subcategories=results["data"].goalSubcategory.docs as Subcategory[] ;
          }
        });
        
    }
    onSubmit(formData) {
      console.log("value -> "+formData+" -- "+formData.name);
          this.goalAttributeService.addGoalAttribute(formData)
      .subscribe(() => {alert("Added");this.redirect()});
    }
    validate(){    
      if(this.goalAttribute.attribute!=undefined && this.goalAttribute.attribute.toString().trim().length==0){
         this.goalAttribute.attribute="";
      }
    }
    redirect() {this.router.navigate(['/goal-attribute-list']); }
  }
