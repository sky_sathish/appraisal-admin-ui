import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { GoalAttributeRoutingModule } from './goal-attribute-form-routing.module';
import { GoalAttributeComponent } from './goal_attribute.component';
import { PageHeaderModule } from './../../../shared';

@NgModule({
    imports: [CommonModule, GoalAttributeRoutingModule, PageHeaderModule,FormsModule],
    declarations: [GoalAttributeComponent]
})
export class GoalAttributeModule {}
