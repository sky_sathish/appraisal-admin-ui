import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoalAttributeComponent } from './goal_attribute.component';

const routes: Routes = [
    {
        path: '', component: GoalAttributeComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GoalAttributeRoutingModule {
}
