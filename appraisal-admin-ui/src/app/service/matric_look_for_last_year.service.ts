import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders  } from '@angular/common/http';

import { of } from 'rxjs';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {MatrixLookForLastYear} from '../model/matric_look_for_last_year.model';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class MatrixLookForLastYearService {
	 api_url='http://localhost:3000/api/matrix-look-for-last-year';

 constructor(private http: HttpClient) { }
  getMatrixLookForLastYear(){	  
    return this.http.get<Object>(this.api_url);	
  }
  
  getMatrixLookForLastYearById(id){	  
    return this.http.get<Object>(this.api_url+"?id="+id);	
	}
	
	addMatrixLookForLastYear (matrixLookForLastYear:MatrixLookForLastYear): Observable<any> {
		console.log("Called addMatrixLookForLastYear"+matrixLookForLastYear.name);
		return this.http.post<MatrixLookForLastYear>(this.api_url, matrixLookForLastYear, httpOptions)
		.pipe(
      tap((matrixLookForLastYear: MatrixLookForLastYear) => 
			console.log("Called matrixLookForLastYear"+matrixLookForLastYear)),
      catchError(this.handleError<MatrixLookForLastYear>('addMatrixLookForLastYear'))
    );
  }
	updateMatrixLookForLastYear (matrixLookForLastYear:MatrixLookForLastYear): Observable<any> {
		console.log("Called updateMatrixLookForLastYear"+matrixLookForLastYear.name);
		return this.http.put<MatrixLookForLastYear>(this.api_url, matrixLookForLastYear, httpOptions)
		.pipe(
      tap((matrixLookForLastYear: MatrixLookForLastYear) => 
			console.log("Called matrixLookForLastYear"+matrixLookForLastYear)),
      catchError(this.handleError<MatrixLookForLastYear>('updateMatrixLookForLastYear'))
    );
  }
  deleteMatrixLookForLastYear(id) {
  
    // return this.http.delete(this.api_url+"/"+id, httpOptions)
    // .pipe(
    //   tap(()=>
    // catchError(this.handleError('deleteMatrixLookForLastYear')))
    // );

    return this.http
    .delete(this.api_url+"/"+id, httpOptions)
    .toPromise()
    .then(res=>{
      })
    .catch(this.handleError).then((data) => { console.log(JSON.stringify(data)); return data },
    (err) => { console.log(err); });;	
  }

	

	private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);
				console.log('${error.message}')
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
	}
  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

	 private log(message: string) {
    console.log('message:'+message)
  }
}