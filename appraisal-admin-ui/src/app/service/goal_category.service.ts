import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders  } from '@angular/common/http';

import { of } from 'rxjs';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {Category} from '../model/goal_category.model';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CategoryService {
	 api_url='http://localhost:3000/api/goal-category';

 constructor(private http: HttpClient) { }
  getCategory(){	  
	  const res = this.http.get<Object>(this.api_url);		
	  return res;
	}
	
	

	addCategory (category:Category): Observable<any> {
		console.log("Called addCategory"+category.name);
		return this.http.post<Category>(this.api_url, category, httpOptions)
		.pipe(
      tap((category: Category) => 
			console.log("Called category"+category)),
      catchError(this.handleError<Category>('addCategory'))
    );
  }
  getCategoryById(id){	  
    return this.http.get<Object>(this.api_url+"?id="+id);	
	}
  updateCategory (category:Category): Observable<any> {
	
		return this.http.put<Category>(this.api_url, category, httpOptions)
		.pipe(
      tap((category: Category) => 
			console.log("Called Category"+category)),
      catchError(this.handleError<Category>('updateCategory'))
    );
  }
  deleteCategory = function(id) {
  
    // return this.http.delete(this.api_url+"/"+id, httpOptions)
    // .pipe(
    //   tap(()=>
    // catchError(this.handleError('deleteCategory')))
    // );
    return this.http
    .delete(this.api_url+"/"+id, httpOptions)
    .toPromise()
    .then(this.extractData)
    .catch(this.handleError).then((data) => { console.log(data); return data },
    (err) => { console.log(err); });
  }


	private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);
				console.log('${error.message}')
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
	}
  private extractData(res: Response) {
    let body = res;
    return body || {};
  }
	 private log(message: string) {
    console.log('message:'+message)
  }
}