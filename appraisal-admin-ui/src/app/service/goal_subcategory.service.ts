import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders  } from '@angular/common/http';

import { of } from 'rxjs';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {Subcategory} from '../model/goal_subcategory.model';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class SubcategoryService {
	 api_goal_attribute_url='http://localhost:3000/api/goal-subcategory';

 constructor(private http: HttpClient) { }
  getSubcategory(){	  
	  const res = this.http.get<Object>(this.api_goal_attribute_url);		
	  return res;
	}
	
	

	addSubcategory (subcategory:Subcategory): Observable<Subcategory> {
		console.log("Called addSubcategory name : "+subcategory.name);
		return this.http.post<Subcategory>(this.api_goal_attribute_url, subcategory, httpOptions)
		.pipe(
      tap((subcategory: Subcategory) => 
			console.log("Called subcategory : "+subcategory)),
      catchError(this.handleError<Subcategory>('addSubcategory'))
    );
  }
  getSubcategoryById(id){	  
    return this.http.get<Object>(this.api_goal_attribute_url+"?id="+id);	
	}
  updateSubcategory (subcategory:Subcategory): Observable<Subcategory> {
	
		return this.http.put<Subcategory>(this.api_goal_attribute_url, subcategory, httpOptions)
		.pipe(
      tap((subcategory: Subcategory) => 
			console.log("Called Subcategory"+subcategory)),
      catchError(this.handleError<Subcategory>('updateSubcategory'))
    );
  }
  deleteSubcategory = function(id) {
  
    // return this.http.delete(this.api_goal_attribute_url+"/"+id, httpOptions)
    // .pipe(
    //   tap(()=>
    // catchError(this.handleError('deleteSubcategory')))
    // );
    return this.http
    .delete(this.api_goal_attribute_url+"/"+id, httpOptions)
    .toPromise()
    .then(res=>{
      this.extractData(res)})
    .catch(this.handleError).then((data) => { console.log(JSON.stringify(data)); return data },
    (err) => { console.log(err); });;	
  }


	private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);
				console.log('${error.message}')
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
	}
  
  private extractData(res: Response) {
    let body = res;
    return body || {};
  }


	 private log(message: string) {
    console.log('message:'+message)
  }
}