import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders  } from '@angular/common/http';

import { of } from 'rxjs';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {FunctionalList} from '../model/functional_list.model';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class FunctionalIDService {

  api_url='http://localhost:3000/api/functional-list';
  public functionalList:FunctionalList[];

  functionids=[];
  constructor(private http: HttpClient) {
    var res = this.http.get<Object>(this.api_url);
    res.subscribe(results => {
     
      if(results!=undefined&&results["data"]!=undefined && results["data"]["docs"]!=undefined){
       
        this.functionalList = results["data"]["docs"] as FunctionalList[];
       
        this.functionalList.forEach(ele =>{

          this.functionids.push(ele.name);
        })

      }
    });
   // this.functionids.push("CS","UX","DESIGN","COPY","UI-TECH-PM","TESTING","SYSTEMS","DM",
    //"PROJECT-COORDINATOR","Client Service","Creative","Marketing","Operations");
    
  }
  
  getFunctionalID(){	  
	  
	  return this.functionids;
  }
  
  getFunctionalList(){
    return this.http
    .get(this.api_url)
    .toPromise()
    .then(this.extractData)
    .catch(this.handleError).then((data) => { console.log(data); return data },
    (err) => { console.log(err); });;	
  }
  
  getFunctionsById(id){	  
    return this.http.get<Object>(this.api_url+"?id="+id);	
  }
  addFunctions (functions:any): Observable<any> {
		console.log(functions.name);
		return this.http.post<any>(this.api_url, functions, httpOptions)
		.pipe(
      tap((functions: any) => 
			console.log(functions)),
      catchError(this.handleError<any>('addRole'))
    );
  }
  autoAddFunctions (functions:any): Observable<any> {
		return this.http.post<any>(this.api_url+"/auto", functions, httpOptions)
		.pipe(
      tap((functions: any) => 
			console.log(functions)),
      catchError(this.handleError<any>('addRole'))
    );
  }
  updatefunctions (functions:any): Observable<any> {
	
		return this.http.put<any>(this.api_url, functions, httpOptions)
		.pipe(
      tap((res: any) => 
			console.log("Called functions"+res)),
      catchError(this.handleError<FunctionalList>('updateRole'))
    );
  }
  deleteFunctions = function(id) {
 
    return this.http
    .delete(this.api_url+"/"+id, httpOptions)
    .toPromise()
    .then(res=>{
      this.extractData(res)})
    .catch(this.handleError).then((data) => { console.log(JSON.stringify(data)); return data },
    (err) => { console.log(err); });;	
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
				console.log('${error.message}')
      return of(result as T);
    };
	}
	
}