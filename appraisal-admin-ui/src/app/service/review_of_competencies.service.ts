import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders  } from '@angular/common/http';
import { of } from 'rxjs';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ReviewOfCompetenciesService {
	 api_url='http://localhost:3000/api/review-of-competencies';

 constructor(private http: HttpClient) { }
  getAllReviewOfCompetencies(){	  
	  // this.http.get<Object>(this.api_url);		
    return this.http
    .get(this.api_url)
    .toPromise()
    .then(this.extractData)
    .catch(this.handleError).then((data) => { return data },
    (err) => { console.log(err); });;	
	}
	
  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

	addReviewOfCompetencies (data:any): Observable<any> {
		return this.http.post<any>(this.api_url, data, httpOptions)
		.pipe(
      tap((data: any) => 
			console.log(data)),
      catchError(this.handleError<any>('addReviewOfCompetencies'))
    );
  }
 
  getReviewOfCompetenciesById(id){	  
    return this.http.get<Object>(this.api_url+"?id="+id);	
    }
    
  updateReviewOfCompetencies (data:any): Observable<any> {
	
		return this.http.put<any>(this.api_url, data, httpOptions)
		.pipe(
      tap((res: any) => 
			console.log("Called data"+res)),
      catchError(this.handleError<any>('updateReviewOfCompetencies'))
    );
  }
  deleteReviewOfCompetencies = function(id) {
  
    return this.http
    .delete(this.api_url+"/"+id, httpOptions)
    .toPromise()
    .then(res=>{
      this.extractData(res)})
    .catch(this.handleError).then((data) => { console.log(JSON.stringify(data)); return data },
    (err) => { console.log(err); });;	
  }

	cloneReviewOfCompetencies (data:any): Observable<any> {
		return this.http.post<any>(this.api_url+"/clone", data, httpOptions)
		.pipe(
      tap((data: any) => 
			console.log(data)),
      catchError(this.handleError<any>('addReviewOfCompetencies'))
    );
  }

	private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);
				console.log('${error.message}')
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
	}
	
	 private log(message: string) {
    console.log('message:'+message)
  }
}