import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable()
export class HomeService {
	constructor(private http: HttpClient) { }
	
  getHomeDeails(){
	  const res = this.http.get<Object>('http://localhost:3000/api/home');		
	  return res;
	}
	
}