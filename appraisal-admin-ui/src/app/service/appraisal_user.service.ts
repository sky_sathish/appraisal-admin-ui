import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders  } from '@angular/common/http';

import { of } from 'rxjs';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {AppraisalUser} from '../model/appraisal_User.model';
import { EmployeeAppraisal } from '../model/employee.appraisal';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AppraisalUserService {
   api_url='http://localhost:3000/api/appraisal-user';
   employee_api_url='http://localhost:3002/api/employee-appraisal';

 constructor(private http: HttpClient) { }
  getAppraisalUser(){	  
	  const res = this.http.get<Object>(this.api_url);		
	  return res;
	}

  addAppraisalUser (appraisalUser:AppraisalUser): Observable<any> {
		return this.http.post<AppraisalUser>(this.api_url, appraisalUser, httpOptions)
		.pipe(
      tap((appraisalUser: AppraisalUser) => 
			console.log("Called AppraisalUser"+appraisalUser)),
      catchError(this.handleError<AppraisalUser>('addAppraisalUser'))
    );
  }
  getAppraisalUserById(id){	  
    return this.http.get<Object>(this.api_url+"?id="+id);	
  }
  getAppraisalById(id){	  
    return this.http.get<Object>(this.api_url+"?appraisal_id="+id);	
  }
  getAppraisalByAppraisalId(id){	  
    return this.http.get<Object>(this.employee_api_url+"?appraisal_id="+id);	
  }
 
  updateAppraisalUser (appraisalUser:AppraisalUser): Observable<any> {
		return this.http.put<AppraisalUser>(this.api_url, appraisalUser, httpOptions)
		.pipe(
      tap((appraisalUser: AppraisalUser) => 
			console.log("Called AppraisalUser"+appraisalUser)),
      catchError(this.handleError<AppraisalUser>('updateAppraisalUser'))
    );
  }

  updateAppraisalUserStatus (appraisalUser:AppraisalUser): Observable<any> {
		return this.http.put<AppraisalUser>(this.api_url, appraisalUser, httpOptions)
		.pipe(
      tap((appraisalUser: AppraisalUser) => 
			console.log("Called AppraisalUser"+appraisalUser)),
      catchError(this.handleError<AppraisalUser>('updateAppraisalUserStatus'))
    );
  }

  updateAppraisalUserBulkList (appraisalUser:any): Observable<any> {
		return this.http.put<AppraisalUser>(this.api_url+"?bk", appraisalUser, httpOptions)
		.pipe(
      tap((appraisalUser: any) => 
			console.log("Called AppraisalUser"+appraisalUser)),
      catchError(this.handleError<any>('updateAppraisalUserBulkList'))
    );
  }
  
  updateEmployee(employeeAppraisal:any): Observable<any> {
		return this.http.put<EmployeeAppraisal>(this.api_url+"?emp", employeeAppraisal, httpOptions)
		.pipe(
      tap((employeeAppraisal: any) => {
        console.log("Called EmployeeAppraisal - "+employeeAppraisal)
        console.log(employeeAppraisal)
      }
      ),
      catchError(this.handleError<AppraisalUser>('updateEmployee'))
    );
  }
  
  deleteAppraisalUserr = function(id) {
  
    return this.http.delete(this.api_url+"/"+id, httpOptions)
    .pipe(
      tap(()=>
    catchError(this.handleError('deleteAppraisalUserr')))
    );
  }

	private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);
				console.log('${error.message}')
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
	}
	
	 private log(message: string) {
    console.log('message:'+message)
  }
}