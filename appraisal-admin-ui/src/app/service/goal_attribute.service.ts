import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders  } from '@angular/common/http';

import { of } from 'rxjs';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {GoalAttribute} from '../model/goal_attribute.model';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class GoalAttributeService {
	 api_goal_attribute_url='http://localhost:3000/api/goal-attribute';

 constructor(private http: HttpClient) { }
  getGoalAttribute(){	  
	  const res = this.http.get<Object>(this.api_goal_attribute_url);		
	  return res;
	}
	
	

	addGoalAttribute (goalAttribute:GoalAttribute): Observable<GoalAttribute> {
		console.log("Called addGoalAttribute"+goalAttribute.attribute);
		return this.http.post<GoalAttribute>(this.api_goal_attribute_url, goalAttribute, httpOptions)
		.pipe(
      tap((goalAttribute: GoalAttribute) => 
			console.log("Called goalAttribute"+goalAttribute)),
      catchError(this.handleError<GoalAttribute>('addGoalAttribute'))
    );
  }
  getGoalAttributeById(id){	  
    return this.http.get<Object>(this.api_goal_attribute_url+"?id="+id);	
	}
  updateGoalAttribute (goalAttribute:GoalAttribute): Observable<GoalAttribute> {
	
		return this.http.put<GoalAttribute>(this.api_goal_attribute_url, goalAttribute, httpOptions)
		.pipe(
      tap((goalAttribute: GoalAttribute) => 
			console.log("Called goalAttribute"+goalAttribute)),
      catchError(this.handleError<GoalAttribute>('updateGoalAttribute'))
    );
  }
  deleteGoalAttribute = function(id) {
  
    // return this.http.delete(this.api_goal_attribute_url+"/"+id, httpOptions)
    // .pipe(
    //   tap(()=>
    // catchError(this.handleError('deleteGoalAttribute')))
    // );
    return this.http
    .delete(this.api_goal_attribute_url+"/"+id, httpOptions)
    .toPromise()
    .then(res=>{
      this.extractData(res)})
    .catch(this.handleError).then((data) => { console.log(JSON.stringify(data)); return data },
    (err) => { console.log(err); });
  }

	private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);
				console.log('${error.message}')
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
	}
  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

	 private log(message: string) {
    console.log('message:'+message)
  }
}