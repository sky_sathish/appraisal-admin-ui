import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders  } from '@angular/common/http';

import { of } from 'rxjs';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {GeneralConfig} from '../model/general_config.model';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class GeneralConfigService {
	 api_goal_attribute_url='http://localhost:3000/api/general-config';

 constructor(private http: HttpClient) { }
  getGeneralConfig(){	  
	  const res = this.http.get<Object>(this.api_goal_attribute_url);		
	  return res;
	}
	
	

	addGeneralConfig (generalConfig:GeneralConfig): Observable<any> {
		console.log("Called addGeneralConfig"+generalConfig.key);
		return this.http.post<GeneralConfig>(this.api_goal_attribute_url, generalConfig, httpOptions)
		.pipe(
      tap((generalConfig: GeneralConfig) => 
			console.log("Called generalConfig"+generalConfig)),
      catchError(this.handleError<GeneralConfig>('addGeneralConfig'))
    );
  }
  getGeneralConfigById(id){	  
    return this.http.get<Object>(this.api_goal_attribute_url+"?id="+id);	
	}
  updateGeneralConfig (generalConfig:GeneralConfig): Observable<any> {
	
		return this.http.put<GeneralConfig>(this.api_goal_attribute_url, generalConfig, httpOptions)
		.pipe(
      tap((generalConfig: GeneralConfig) => 
			console.log("Called GeneralConfig"+generalConfig)),
      catchError(this.handleError<GeneralConfig>('updateGeneralConfig'))
    );
  }
  deleteGeneralConfig = function(id) {
  
    return this.http.delete(this.api_goal_attribute_url+"/"+id, httpOptions)
    .pipe(
      tap(()=>
    catchError(this.handleError('deleteGeneralConfig')))
    );
  }

	private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);
				console.log('${error.message}')
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
	}
	
	 private log(message: string) {
    console.log('message:'+message)
  }
}