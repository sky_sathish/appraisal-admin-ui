import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders  } from '@angular/common/http';

import { of } from 'rxjs';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {ApprisalFunctionalAttribute} from '../model/appraisal_functional_attributes.model';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ApprisalFunctionalAttributeService {
	 api_goal_attribute_url='http://localhost:3000/api/appraisal-functional-attribute';

 constructor(private http: HttpClient) { }
  getApprisalFunctionalAttribute(){	  
	  const res = this.http.get<Object>(this.api_goal_attribute_url);		
	  return res;
	}
	
	

	addApprisalFunctionalAttribute (apprisalFunctionalAttribute:ApprisalFunctionalAttribute): Observable<ApprisalFunctionalAttribute> {
		console.log("Called addApprisalFunctionalAttribute"+apprisalFunctionalAttribute.function_id);
		return this.http.post<ApprisalFunctionalAttribute>(this.api_goal_attribute_url, apprisalFunctionalAttribute, httpOptions)
		.pipe(
      tap((apprisalFunctionalAttribute: ApprisalFunctionalAttribute) => 
			console.log("Called apprisalFunctionalAttribute"+apprisalFunctionalAttribute)),
      catchError(this.handleError<ApprisalFunctionalAttribute>('addApprisalFunctionalAttribute'))
    );
  }
  getApprisalFunctionalAttributeById(id){	  
    return this.http.get<Object>(this.api_goal_attribute_url+"?id="+id);	
	}
  updateApprisalFunctionalAttribute (apprisalFunctionalAttribute:ApprisalFunctionalAttribute): Observable<ApprisalFunctionalAttribute> {
	
		return this.http.put<ApprisalFunctionalAttribute>(this.api_goal_attribute_url, apprisalFunctionalAttribute, httpOptions)
		.pipe(
      tap((apprisalFunctionalAttribute: ApprisalFunctionalAttribute) => 
			console.log("Called ApprisalFunctionalAttribute"+apprisalFunctionalAttribute)),
      catchError(this.handleError<ApprisalFunctionalAttribute>('updateApprisalFunctionalAttribute'))
    );
  }
  
  deleteApprisalFunctionalAttributer = function(id) {
  
    // return this.http.delete(this.api_goal_attribute_url+"/"+id, httpOptions)
    // .pipe(
    //   tap(()=>
    // catchError(this.handleError('deleteApprisalFunctionalAttributer')))
    // );

    return this.http
    .delete(this.api_goal_attribute_url+"/"+id, httpOptions)
    .toPromise()
    .then(res=>{
      this.extractData(res)})
    .catch(this.handleError).then((data) => { console.log(JSON.stringify(data)); return data },
    (err) => { console.log(err); });;	
  }

	private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);
				console.log('${error.message}')
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
	}
		
  private extractData(res: Response) {
    let body = res;
    return body || {};
  }
	 private log(message: string) {
    console.log('message:'+message)
  }
}