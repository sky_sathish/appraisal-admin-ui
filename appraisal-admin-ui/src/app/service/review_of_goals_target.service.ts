import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders  } from '@angular/common/http';
import { of } from 'rxjs';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ReviewOfGoalsTargetService {
	 api_url='http://localhost:3000/api/review-of-goals-target';

 constructor(private http: HttpClient) { }

	addReviewOfGoalsTarget (data:any): Observable<any> {
		return this.http.post<any>(this.api_url, data, httpOptions)
		.pipe(
      tap((data: any) => 
			console.log(data)),
      catchError(this.handleError<any>('addReviewOfGoalsTarget'))
    );
  }
  updateReviewOfGoalsTarget (data:any): Observable<any> {
		return this.http.put<any>(this.api_url, data, httpOptions)
		.pipe(
      tap((res: any) => 
			console.log("Called data"+res)),
      catchError(this.handleError<any>('updateReviewOfGoalsTarget'))
    );
  }
  deleteReviewOfGoalsTarget = function(id) {
    return this.http
    .delete(this.api_url+"/"+id, httpOptions)
    .toPromise()
    .then(res=>{
      this.extractData(res)})
    .catch(this.handleError).then((data) => { console.log(JSON.stringify(data)); return data },
    (err) => { console.log(err); });;	
  }

	private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);
				console.log('${error.message}')
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
	}
	
	 private log(message: string) {
    console.log('message:'+message)
  }
}