import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders  } from '@angular/common/http';

import { of } from 'rxjs';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {Role} from '../model/role.model';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class RoleService {
	 api_url='http://localhost:3000/api/roles';

 constructor(private http: HttpClient) { }
  getRole(){	  
	  // this.http.get<Object>(this.api_url);		
    return this.http
    .get(this.api_url)
    .toPromise()
    .then(this.extractData)
    .catch(this.handleError).then((data) => { return data },
    (err) => { console.log(err); });;	
	}
	
  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

	addRole (role:any): Observable<any> {
		console.log(role.name);
		return this.http.post<any>(this.api_url, role, httpOptions)
		.pipe(
      tap((role: any) => 
			console.log(role)),
      catchError(this.handleError<any>('addRole'))
    );
  }
  autoAddRoles (role:any): Observable<any> {
		console.log(role.name);
		return this.http.post<any>(this.api_url+"/auto", role, httpOptions)
		.pipe(
      tap((role: any) => 
			console.log(role)),
      catchError(this.handleError<any>('addRole'))
    );
  }
  getRoleById(id){	  
    return this.http.get<Object>(this.api_url+"?id="+id);	
	}
  updateRole (role:any): Observable<any> {
	
		return this.http.put<any>(this.api_url, role, httpOptions)
		.pipe(
      tap((res: any) => 
			console.log("Called Role"+res)),
      catchError(this.handleError<Role>('updateRole'))
    );
  }
  deleteRole = function(id) {
  
    // return this.http.delete(this.api_url+"/"+id, httpOptions)
    // .pipe(
    //   tap(()=>
    // catchError(this.handleError('deleteRole')))
    // );
    return this.http
    .delete(this.api_url+"/"+id, httpOptions)
    .toPromise()
    .then(res=>{
      this.extractData(res)})
    .catch(this.handleError).then((data) => { console.log(JSON.stringify(data)); return data },
    (err) => { console.log(err); });;	
  }


	private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);
				console.log('${error.message}')
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
	}
	
	 private log(message: string) {
    console.log('message:'+message)
  }
}