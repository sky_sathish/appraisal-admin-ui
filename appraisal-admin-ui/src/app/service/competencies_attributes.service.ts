import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders  } from '@angular/common/http';

import { of } from 'rxjs';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {CompetenciesAttribute} from '../model/competencies_attribute.model';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CompetenciesAttributeService {
	 api_goal_attribute_url='http://localhost:3000/api/competencies-attribute';

 constructor(private http: HttpClient) { }
  getCompetenciesAttribute(){	  
	  const res = this.http.get<Object>(this.api_goal_attribute_url);		
	  return res;
	}
	
	

	addCompetenciesAttribute (competenciesAttribute:CompetenciesAttribute): Observable<any> {
		console.log("Called addCompetenciesAttribute"+competenciesAttribute.attribute);
		return this.http.post<CompetenciesAttribute>(this.api_goal_attribute_url, competenciesAttribute, httpOptions)
		.pipe(
      tap((competenciesAttribute: CompetenciesAttribute) => 
			console.log("Called competenciesAttribute"+competenciesAttribute)),
      catchError(this.handleError<CompetenciesAttribute>('addCompetenciesAttribute'))
    );
  }
  getCompetenciesAttributeById(id){	  
    return this.http.get<Object>(this.api_goal_attribute_url+"?id="+id);	
	}
  updateCompetenciesAttribute (competenciesAttribute:CompetenciesAttribute): Observable<any> {
	
		return this.http.put<CompetenciesAttribute>(this.api_goal_attribute_url, competenciesAttribute, httpOptions)
		.pipe(
      tap((competenciesAttribute: CompetenciesAttribute) => 
			console.log("Called CompetenciesAttribute"+competenciesAttribute)),
      catchError(this.handleError<CompetenciesAttribute>('updateCompetenciesAttribute'))
    );
  }
  deleteCompetenciesAttribute = function(id) {
  
    return this.http.delete(this.api_goal_attribute_url+"/"+id, httpOptions)
    .pipe(
      tap(()=>
    catchError(this.handleError('deleteCompetenciesAttribute')))
    );
  }
	private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);
				console.log('${error.message}')
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
	}
	
	 private log(message: string) {
    console.log('message:'+message)
  }
}