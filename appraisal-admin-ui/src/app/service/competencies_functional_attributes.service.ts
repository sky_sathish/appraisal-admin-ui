import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders  } from '@angular/common/http';

import { of } from 'rxjs';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {CompetenciesFunctionalAttributes} from '../model/competencies_functional_attributes.model';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CompetenciesFunctionalAttributesService {
	 api_goal_attribute_url='http://localhost:3000/api/competencies-functional-attributes';

 constructor(private http: HttpClient) { }
  getCompetenciesFunctionalAttributes(){	  
	  const res = this.http.get<Object>(this.api_goal_attribute_url);		
	  return res;
	}
	
	

	addCompetenciesFunctionalAttributes (competenciesFunctionalAttributes:CompetenciesFunctionalAttributes): Observable<CompetenciesFunctionalAttributes> {
		return this.http.post<CompetenciesFunctionalAttributes>(this.api_goal_attribute_url, competenciesFunctionalAttributes, httpOptions)
		.pipe(
      tap((competenciesFunctionalAttributes: CompetenciesFunctionalAttributes) => 
			console.log("Called competenciesFunctionalAttributes"+competenciesFunctionalAttributes)),
      catchError(this.handleError<CompetenciesFunctionalAttributes>('addCompetenciesFunctionalAttributes'))
    );
  }
  getCompetenciesFunctionalAttributesById(id){	  
    return this.http.get<Object>(this.api_goal_attribute_url+"?id="+id);	
	}
  updateCompetenciesFunctionalAttributes (competenciesFunctionalAttributes:CompetenciesFunctionalAttributes): Observable<CompetenciesFunctionalAttributes> {
	
		return this.http.put<CompetenciesFunctionalAttributes>(this.api_goal_attribute_url, competenciesFunctionalAttributes, httpOptions)
		.pipe(
      tap((competenciesFunctionalAttributes: CompetenciesFunctionalAttributes) => 
			console.log("Called CompetenciesFunctionalAttributes"+competenciesFunctionalAttributes)),
      catchError(this.handleError<CompetenciesFunctionalAttributes>('updateCompetenciesFunctionalAttributes'))
    );
  }
  deleteCompetenciesFunctionalAttributes = function(id) {
  
    return this.http.delete(this.api_goal_attribute_url+"/"+id, httpOptions)
    .pipe(
      tap(()=>
    catchError(this.handleError('deleteCompetenciesFunctionalAttributes')))
    );
  }

	private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);
				console.log('${error.message}')
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
	}
	
	 private log(message: string) {
    console.log('message:'+message)
  }
}