import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders  } from '@angular/common/http';

import { of } from 'rxjs';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {AppraisalMaster} from '../model/appraisal_master.model';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AppraisalMasterService {
	 api_url='http://localhost:3000/api/appraisal';

 constructor(private http: HttpClient) { }


   getAppraisalMaster() :Promise<any>{	  
    return this.http
    .get(this.api_url)
    .toPromise()
    .then(this.extractData)
    .catch(this.handleError).then((data) => { console.log(data); return data["data"] },
    (err) => { console.log(err); });;		
	}
	
  private extractData(res: Response) {
    console.log(res);
    let body = res;
    return body || {};
}

	addAppraisalMaster (appraisalMaster:AppraisalMaster): Observable<any> {

		return this.http.post<AppraisalMaster>(this.api_url, appraisalMaster, httpOptions)
		.pipe(
      tap((appraisalMaster: AppraisalMaster) => 
			console.log("Called AppraisalMaster"+appraisalMaster)),
      catchError(this.handleError<AppraisalMaster>('addAppraisalMaster'))
    );
  }
  getAppraisalMasterById(id){	  
    return this.http.get<Object>(this.api_url+"?id="+id);	
	}
  updateAppraisalMaster (appraisalMaster:AppraisalMaster): Observable<any> {
	
		return this.http.put<AppraisalMaster>(this.api_url, appraisalMaster, httpOptions)
		.pipe(
      tap((appraisalMaster: AppraisalMaster) => 
			console.log("Called AppraisalMaster"+appraisalMaster)),
      catchError(this.handleError<AppraisalMaster>('updateAppraisalMaster'))
    );
  }
  
  deleteAppraisalMasterr = function(id) {
  
    // return this.http.delete(this.api_url+"/"+id, httpOptions)
    // .pipe(
    //   tap(()=>
    // catchError(this.handleError('deleteAppraisalMasterr')))
    // );

    return this.http
    .delete(this.api_url+"/"+id, httpOptions)
    .toPromise()
    .then(res=>{
      this.extractData(res)})
    .catch(this.handleError).then((data) => { console.log(JSON.stringify(data)); return data },
    (err) => { console.log(err); });;	
  }

	private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);
				console.log('${error.message}')
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
	}
	
	 private log(message: string) {
    console.log('message:'+message)
  }
}