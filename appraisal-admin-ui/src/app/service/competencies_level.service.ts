import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders  } from '@angular/common/http';

import { of } from 'rxjs';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {CompetenciesLevel} from '../model/competencies_level.model';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CompetenciesLevelService {
	 api_url='http://localhost:3000/api/competencies_level';

 constructor(private http: HttpClient) { }
  getCompetenciesLevel(){	  
	  const res = this.http.get<Object>(this.api_url);		
	  return res;
	}
	
	

	addCompetenciesLevel (competenciesLevel:CompetenciesLevel): Observable<any> {
		console.log("Called addcompetenciesLevel"+competenciesLevel.level_name);
		return this.http.post<CompetenciesLevel>(this.api_url, competenciesLevel, httpOptions)
		.pipe(
      tap((competenciesLevel: CompetenciesLevel) => 
			console.log("Called competenciesLevel"+competenciesLevel)),
      catchError(this.handleError<CompetenciesLevel>('addCompetenciesLevel'))
    );
  }
  getCompetenciesLevelById(id){	  
    return this.http.get<Object>(this.api_url+"?id="+id);	
	}
  updateCompetenciesLevel (competenciesLevel:CompetenciesLevel): Observable<any> {
	
		return this.http.put<CompetenciesLevel>(this.api_url, competenciesLevel, httpOptions)
		.pipe(
      tap((competenciesLevel: CompetenciesLevel) => 
			console.log("Called competenciesLevel"+competenciesLevel)),
      catchError(this.handleError<CompetenciesLevel>('updateCompetenciesLevel'))
    );
  }
  deleteCompetenciesLevel = function(id) {
  
    return this.http.delete(this.api_url+"/"+id, httpOptions)
    .pipe(
      tap(()=>
    catchError(this.handleError('deleteCompetenciesLevel')))
    );
  }

	private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);
				console.log('${error.message}')
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
	}
	
	 private log(message: string) {
    console.log('message:'+message)
  }
}