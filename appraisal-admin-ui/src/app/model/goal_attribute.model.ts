export class GoalAttribute {
    _id: string;
    attribute:string;
    attribute_category:{"_id":String,"name":String};
    attribute_sub_category:{"_id":String,"name":String};
  }