export class CompetenciesFunctionalAttributes {
    _id: String;
    behavioral_competencies:String;
    competency_level:String;
    weightage:Number;
    weightage_unit:String;
    role_id:{"_id":String,"name": String};
    function_id:String;
  }