export class AppraisalMaster {
    _id: String;
    appraisal_name:String;
    appraisal_intro:String;
    type: Number;
    due_date: Date;
    status: String;
    created_date: Date;
    modified_date: Date;
    created_by: String;
    modified_by: String;
    islocked: Boolean;
  }


 
    