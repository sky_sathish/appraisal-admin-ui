export class AppraisalUser {
    _id: String;
    appraisal_id: String;
    first_name: String;
    last_name: String;
    emp_code: String;
    gender: String;
    hash: String;
    email_id: String;
    group_emp: String;
    doj: Date;
    designation: String;
    functional_area: String;
    rg_group: String;
    supervisor_name: String;
    supervisor_code: String;
    supervisor_hash: String;
    supervisor_email_id: String;
    supervisor_gender: String;
    supervisor_rg_group: String;
    appraiser_id:String;
    appraiser_name: String;
    reviewer_id:String;
    reviewer_name: String;
    status: String;
  }


 
    