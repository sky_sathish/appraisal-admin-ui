export class EmployeeAppraisal {
    appraisal_id: String;
    employee_id: String;
    employee_name: String;
    employee_code: String;
    designation: String;
    supervisor: String;
    appraiser_name: String;
    appraiser_id: String;
    reviewer_name: String;
    reviewer_id: String;
  }


 
    