export class ApprisalFunctionalAttribute {
    _id: String;
    function_id:String;
    role_id:String;
    category:String;
    subcategory:any;
    description:String;
    target_for_last_year:String;
    target_for_last_year_unit:String;
    target_for_last_year_description:String;
    target_for_next_year:String;
    target_for_next_year_unit:String;
    target_for_next_year_description:String;
    weightage:Number;
  }


 
    