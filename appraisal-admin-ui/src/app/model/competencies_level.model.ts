export class CompetenciesLevel {
  _id: String;
  level_name: String;
  level_desc: String;
  sequence: String;
  weightage: String;
  rating_scale: String;
  scenario_1: String;
  scenario_2: String;
}