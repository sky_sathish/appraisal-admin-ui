export class ReviewOfCompetenciesLevelModel {
  
    _id: String;
    review_competencies_id: String;
    competencies_level:{"_id":String,"name":String};
    weightage:String;
    year:String;
    term:String;
    active_status:Boolean;
      
  }