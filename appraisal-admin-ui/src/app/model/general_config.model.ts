export class GeneralConfig {
    _id: string;
    key:String;
    key_desc:String;
    value:String;
    tag:String;
    sequence:Number;
    description:String;
  }